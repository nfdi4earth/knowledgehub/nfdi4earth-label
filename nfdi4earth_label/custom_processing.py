#!/usr/bin/env python

# Custom processing functions for nfdi4earth_label. These functions take the OAI-PMH response returned by a repository (typically for a single record)
# and analyse it to determine whether certain FAIR standards and conditions are met.

import logging
import re

logger = logging.getLogger(__name__)


def custom_analyze_records(records: list) -> dict:
    """
    Perform custom analysis of OAI-PHM records to determine whether certain FAIR standards and conditions are met.

    @param records: OAI-PMH records, i.e., a list of records, each entry is the result of a call to ```sickle.GetRecord()```. This is a list because
    we typically randomly sample a number of records from a repository.
    """

    custom_result = {
        "contains_orcid": False,
        "contains_ror": False,
    }

    if len(records) == 0:
        logger.warning(f"custom_analyze_records: No records to analyze")
        return custom_result

    # ORCID pattern: https://support.orcid.org/hc/en-us/articles/360006897674-Structure-of-the-ORCID-Identifier
    contains_orcid_pattern = re.compile(r"\d{4}-\d{4}-\d{4}-\d{3}[0-9X]")

    # ROR pattern for research organization identifiers:
    ror_pattern = re.compile(r"^https://ror.org/")

    for item_idx, item in enumerate(records):
        item_num = item_idx + 1
        logger.debug(f"Custom analysis of record #{item_num} of {len(records)}")
        logger.debug(item)
        identifier = item.header.identifier
        datestamp = item.header.datestamp
        logger.debug(f" -Item header information: identifier: {identifier}, datestamp: {datestamp}")
        metadata = item.metadata
        # fields = ["dc:title", "dc:creator", "dc:source", "dc:publisher", "dc:date", "dc:type", "dc:format", "dc:identifier", "dc:language", "dc:relation", "dc:rights"]
        logger.debug(f" -type(metadata): {type(metadata)} with keys: {metadata.keys()}")

        potential_orcid_fields = ["creator", "contributor"]
        for field_name in potential_orcid_fields:
            if field_name in metadata:
                # check whether the field is iterable (e.g., a list of authors)
                try:
                    _ = iter(field_name)
                    field = metadata[field_name]
                except TypeError:  # not iterable, convert to (iterable) list
                    field = [metadata[field_name]]

            for _, value in enumerate(field):
                found = contains_orcid_pattern.search(value)
                if found is not None:
                    logger.debug(f" -Found ORCID in field '{field_name}' of record #{item_num}: '{found.group(0)}'")
                    custom_result["contains_orcid"] = True

        potential_ror_fields = ["source", "publisher", "relation"]
        for field_name in potential_ror_fields:
            if field_name in metadata:
                # check whether the field is iterable (e.g., a list of authors)
                try:
                    _ = iter(field_name)
                    field = metadata[field_name]
                except TypeError:
                    field = [metadata[field_name]]

            for _, value in enumerate(field):
                found = ror_pattern.search(value)
                if found is not None:
                    logger.debug(f" -Found ROR in field '{field_name}' of record #{item_num}: '{found.group(0)}'")
                    custom_result["contains_ror"] = True

        source = metadata.get("source", None)
        creator = metadata.get("creator", None)
        title = metadata.get("title", None)
        logger.debug(f" -Item metadata: author: {creator}, source: {source}, title: {title}")

        return custom_result

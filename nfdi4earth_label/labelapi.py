#!/usr/bin/env python

"""
FastAPI-based Label API.
"""

import logging
from fastapi import FastAPI, HTTPException, Header
from typing import Any, Dict, Optional
from pydantic import BaseModel
from jose import jwk, jwt, exceptions
import html
import requests

from starlette.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, Response

from random import randrange
import json
from .common import generate_uuid, verify_could_be_subassessment, get_eval_default_selfassessment_entry
from .assessment import SubAsmType, get_subassessment_with_uuid
from .cordra_util import update_cordra_object, get_cordra_cfg, compute_achievedN4ELabel_for_assessments, get_all_assessments_from_cordra, get_repo_id_for_name
from .subasm_scoring import metrics, non_metrics

# Use slowapi to limit the number of requests to the API.
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address
from slowapi.errors import RateLimitExceeded

logger = logging.getLogger(__name__)


from .cordra_util import (
    save_selfassessment_to_cordra,
    repo_exists_in_cordra,
    get_unique_repo_name_for_id,
    save_re3data_assessment_to_cordra,
    save_fuji_assessment_to_cordra,
    get_repo_oai_pmh_api_url,
    ensure_repo_assessment_in_cordra,
    get_official_eval_instances
)
from .re3data import compute_repo_re3data_eval_result
from .fuji_data import compute_repo_fuji_eval_result, FujiEvalStatus


from .settings import get_settings

settings = get_settings()
FUJI_API_URL = settings.get("FUJI_API_URL")
CORS_ORIGINS = settings.get("CORS_ORIGINS")
DISABLE_AUTH_FOR_TESTING = settings.get("DISABLE_AUTH_FOR_TESTING")
AUTHZ_ASSESS_REPO_ATTRIBUTE = settings.get("AUTHZ_ASSESS_REPO_ATTRIBUTE")
AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE = settings.get("AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE")
KEYCLOAK_CONFIG_JSON_FILEPATH = settings.get("KEYCLOAK_CONFIG_JSON_FILEPATH")
FRONTEND_VERIFY_URL = settings.get("FRONTEND_VERIFY_URL")
BACKEND_BASE_URL = settings.get("BACKEND_BASE_URL")


from .labelmail import send_label_contact_email, send_label_application_as_repository_representative_email

limiter = Limiter(key_func=get_remote_address)

app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_ORIGINS,  # Allow CORS from the following origins.
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.state.limiter = limiter
app.add_exception_handler(RateLimitExceeded, _rate_limit_exceeded_handler)

if not DISABLE_AUTH_FOR_TESTING:
    with open(KEYCLOAK_CONFIG_JSON_FILEPATH) as file:
        keycloak_config = file.read()
    authConfig = json.loads(keycloak_config)
    public_key = jwk.construct(authConfig["jwk"])
else:
    authConfig = dict()
    public_key = ""


def authenticate_and_decode_token(token: str)->dict:
    auth_success = False
    if isinstance(token, str) and token.startswith("Bearer "):
        try:
            token = token.replace("Bearer ", "").strip()
            audience=authConfig["aud"][0]
            #print(f"keycloak_auth: audience='{audience}', token='{token}'")
            decoded_token = jwt.decode(
                token,
                public_key,
                algorithms=[authConfig["jwk"]["alg"]],
                audience=audience,
            )
        except (exceptions.ExpiredSignatureError, exceptions.JWTError):
            raise HTTPException(status_code=401, detail="Authentication failed")
        if "iss" in decoded_token and decoded_token["iss"] == authConfig["iss"]:
            auth_success = True
            return decoded_token
    if not auth_success:
        raise HTTPException(status_code=401, detail="Authentication failed")


def check_user_authorized_to_request_for_repo(authorization: str, repo_cordra_id: str) -> None:
    # Returns none if success, otherwise raises exception
    user_info = authenticate_and_decode_token(authorization)
    is_authorized = False
    if AUTHZ_ASSESS_REPO_ATTRIBUTE in user_info:
        allowed_repo_ids = user_info[AUTHZ_ASSESS_REPO_ATTRIBUTE]
        if isinstance(allowed_repo_ids, list) and repo_cordra_id in allowed_repo_ids:
            is_authorized = True
            logger.info(f"check_user_authorized_to_request_for_repo: okay, user authorized for repo with ID {repo_cordra_id}.")
    if not is_authorized:
        logger.warning(f"check_user_authorized_to_request_for_repo: FAILED, user not authorized for repo  with ID {repo_cordra_id}.")
        raise HTTPException(status_code=403, detail="Not authorized to assess this repository")


def check_user_authorized_as_labelteammember(authorization: str) -> None:
    # Returns none if success, otherwise raises exception
    user_info = authenticate_and_decode_token(authorization)
    is_authorized = False
    if AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE in user_info:
        labelteammember_status = user_info[AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE]
        if labelteammember_status == "yes":
            is_authorized = True
            logger.info(f"check_user_authorized_as_labeladmin: okay, user authorized as label team member.")
    if not is_authorized:
        logger.warning(f"check_user_authorized_as_labeladmin: FAILED, user not authorized as label team member.")
        raise HTTPException(status_code=403, detail="Not authorized ass label team member, cannot change subassessment status")



def check_user_logged_in(authorization: str) -> None:
    # Returns none if success, otherwise raises exception
    _ = authenticate_and_decode_token(authorization)


class StartRe3dataAssessmentRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    repoid: str


@app.post("/request_re3data_assessment")
async def request_re3data_assessment(
    request: StartRe3dataAssessmentRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:
    """
    Endpoint to request a re3data evaluation of a repository by ID, and the storage of the result in the database.
    @return Returns JSON with the evaluation result.
    """
    repo_cordra_id = request.repoid
    if not DISABLE_AUTH_FOR_TESTING:
        check_user_authorized_to_request_for_repo(authorization, repo_cordra_id)

    if not repo_exists_in_cordra(repo_cordra_id):
        return { "repo_id": repo_cordra_id, "re3data_status" : "error", "re3data_message": "No repository with the given identifier exists in the database." }


    repo_name = get_unique_repo_name_for_id(repo_cordra_id)
    if repo_name is None:
        return { "repo_id": repo_cordra_id, "re3data_status" : "error", "re3data_message": "Could not retrieve repo name for repository." }

    re3data_status_string = "Computing re3data assessment failed."
    re3data_eval_result = None
    try:
        label_eval_result = compute_repo_re3data_eval_result(repo_cordra_id, repo_name)
        re3data_eval_result = label_eval_result["results"]["re3data"][0]
        # Add UUID for this new assessment.
        re3data_eval_result["uuid"] = generate_uuid()
    except Exception as _:
        return { "repo_id": repo_cordra_id, "repo_name": repo_name, "re3data_status" : "error", "re3data_message": re3data_status_string}

    try:
        re3data_status_string = "Re3data assessment computed and saved."
        save_re3data_assessment_to_cordra(repo_cordra_id, re3data_eval_result)
        _, full_assessment = ensure_repo_assessment_in_cordra(repo_cordra_id)
        return { "repo_id": repo_cordra_id, "repo_name": repo_name, "re3data_status" : "ok", "re3data_message": re3data_status_string, "re3data_eval_result": re3data_eval_result, "assessment_id": full_assessment["id"] }
    except Exception as _:
        re3data_status_string = "Saving the computed re3data assessment failed."
        return { "repo_id": repo_cordra_id, "repo_name": repo_name, "re3data_status" : "error", "re3data_message": re3data_status_string}


class StartFujiAssessmentRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    repoid: str


@app.post("/request_fuji_assessment")
def request_fuji_assessment(
    request: StartFujiAssessmentRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:
    """
    Endpoint to request a F-UJI evaluation of a repository by ID, and the storage of the result in the database.
    @return Returns JSON with the evaluation result.
    """
    repo_cordra_id = request.repoid
    if not DISABLE_AUTH_FOR_TESTING:
        check_user_authorized_to_request_for_repo(authorization, repo_cordra_id)

    if not repo_exists_in_cordra(repo_cordra_id):
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": "No repository with the given identifier exists in the database." }

    apiurl = get_repo_oai_pmh_api_url(repo_cordra_id)
    if apiurl is None:
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": "Repository evaluation failed: no API URL found for repository." }

    if not apiurl.startswith("http"):
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": "Repository evaluation failed: no API URL found for repository." }


    repo_name = get_unique_repo_name_for_id(repo_cordra_id)
    if repo_name is None:
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": "Could not retrieve repo name for repository." }

    fuji_eval_result = None
    fuji_status_string = "Computing F-UJI assessment failed."
    fuji_json_details = FujiEvalStatus.getDetailJsonTemplate()
    try:
        fuji_eval_result, fuji_status_string, fuji_json_details  = compute_repo_fuji_eval_result(repo_url=apiurl, repo_name=repo_name, selection_n=3)
    except Exception as _:
        #raise HTTPException(status_code=500, detail=f"Computing F-UJI assessment failed.")
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": fuji_status_string, "fuji_json_details": fuji_json_details }

    if fuji_eval_result is None:
        return { "repo_id": repo_cordra_id, "fuji_status" : "error", "fuji_message": fuji_status_string, "fuji_json_details": fuji_json_details }
        #raise HTTPException(status_code=500, detail=f"Repository evaluation failed.")
    else:
        try:
            fuji_eval_result["uuid"] = generate_uuid()
            save_fuji_assessment_to_cordra(repo_cordra_id, fuji_eval_result)
            _, full_assessment = ensure_repo_assessment_in_cordra(repo_cordra_id)
            return { "repo_id": repo_cordra_id, "repo_name": repo_name, "fuji_status" : "ok", "fuji_message": fuji_status_string, "fuji_eval_result": fuji_eval_result, "assessment_id": full_assessment["id"], "fuji_json_details": fuji_json_details }
        except Exception as _:
            fuji_status_string = "Saving the computed F-UJI assessment failed."
        return { "repo_id": repo_cordra_id, "repo_name": repo_name, "fuji_status" : "error", "fuji_message": fuji_status_string, "fuji_json_details": fuji_json_details}



class SelfAssessmentRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    repoid: str
    assessment: Dict[Any, Any]


@app.post("/submit_selfassessment")
def submit_selfassessment(
    request: SelfAssessmentRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:
    """
    Endpoint to submit a JSON structure containing the self assessment from the selfassessment form.
    @param repo_cordra_id: The repository cordra ID.
    @return Returns JSON with a status message, describing whether the storage of the selfassessment was accepted and worked out.
    """

    repo_cordra_id = request.repoid
    assessment_info_frontend = request.assessment

    selfassessment = get_eval_default_selfassessment_entry()

    # See selfassessment form in frontend for the expected fields.
    expected_assessment_metric_fields = list(selfassessment["metrics"].keys())
    missing_fields = []
    for metrics_field in expected_assessment_metric_fields:
        if not metrics_field in assessment_info_frontend:
            missing_fields.append(metrics_field)
    if len(missing_fields) > 0:
        raise HTTPException(status_code=422, detail=f"Invalid 'assessment' received from frontend selfassessment form: missing fields: {missing_fields}.")



    # Copy the relevant fields from the received assessment_info to the default selfassessment struct.
    try:
        for metrics_field in expected_assessment_metric_fields:
            print("handling metrics field: " + metrics_field)
            frontend_result = assessment_info_frontend[metrics_field]["result"]
            selfassessment["metrics"][metrics_field]["result"] = frontend_result
            if frontend_result == True:
                frontend_description = assessment_info_frontend[metrics_field]["reason"]["description"]
                selfassessment["metrics"][metrics_field]["reason"]["description"] = frontend_description
    except Exception as ex:
        logger.error(f"submit_selfassessment: failed to copy fields from frontend to selfassessment struct, most likely because they were missing in POST data from frontend: {ex}")
        raise HTTPException(status_code=422, detail=f"Storing selfassessment failed: invalid selfassessment struct received, missing required standard fields.")

    # The lines above only copy the essential fields result and reason.description that are shared by all metrics.
    # We must also copy the other fields that are specific to each metric.

    metrics_and_nonmetrics = metrics.copy(); # Need to merge for the non-metric 'hasCertifications' field which we also need to copy.
    for key in non_metrics["selfassessment"]:
        metrics_and_nonmetrics["selfassessment"][key] = non_metrics["selfassessment"][key].copy()

    for metrics_field in expected_assessment_metric_fields:
        extra_field_name = "unknown"
        try:
            metric_reason_extra_fieldnames = list(metrics_and_nonmetrics["selfassessment"][metrics_field]["extra_reason_fields"].keys()) if "extra_reason_fields" in metrics_and_nonmetrics["selfassessment"][metrics_field] else []
            for extra_field_name in metric_reason_extra_fieldnames:
                if extra_field_name in list(assessment_info_frontend[metrics_field]["reason"].keys()):
                    selfassessment["metrics"][metrics_field]["reason"][extra_field_name] = assessment_info_frontend[metrics_field]["reason"][extra_field_name]
        except Exception as ex:
            logger.error(f"submit_selfassessment: failed to copy extra reason field '{extra_field_name}' of metric '{metrics_field}' from frontend to selfassessment struct, most likely because it was missing in POST data from frontend: {ex}")
            raise HTTPException(status_code=422, detail=f"Storing selfassessment failed: invalid selfassessment struct received, missing required reason field.")

    if not verify_could_be_subassessment(selfassessment, "selfassessment"):
        raise HTTPException(status_code=422, detail=f"Storing selfassessment failed: invalid selfassessment struct received.")

    if not DISABLE_AUTH_FOR_TESTING:
        check_user_authorized_to_request_for_repo(authorization, repo_cordra_id)

    if not repo_exists_in_cordra(repo_cordra_id):
        raise HTTPException(status_code=422, detail=f"Storing selfassessment failed: no such repo.")

    try:
        save_selfassessment_to_cordra(repo_cordra_id, selfassessment)
        _, full_assessment = ensure_repo_assessment_in_cordra(repo_cordra_id)
        return {"status": "okay", "repo_id": repo_cordra_id, "assessment_id": full_assessment["id"]}
    except Exception as ex:
        print(f"submit_selfassessment: save_selfassessment_to_cordra failed with exception: {ex}")
        raise HTTPException(status_code=500, detail=f"Storing selfassessment failed: internal error.")


class ApplicationRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    submitter: Dict[Any, Any]
    repoid: str


# Submit aaplication to be approved as repository representative. This is triggered via the respective form for logged
# in users in the Label web frontend.
@app.post("/submit_application")
def submit_application(
    request: ApplicationRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:

    submitter_name = request.submitter.name
    submitter_email = request.submitter.email
    submitter_organization = request.submitter.organization
    repo_cordra_id = request.repoid

    if not '@' in submitter_email:
        raise HTTPException(status_code=500, detail=f"Submitting contact form failed: Invalid 'submitter_email' email address.")

    if not DISABLE_AUTH_FOR_TESTING:
        check_user_logged_in(authorization=authorization)

    if not repo_exists_in_cordra(repo_cordra_id):
        raise HTTPException(status_code=500, detail=f"Application as repository representative failed: no such repo.")

    try:
        send_label_application_as_repository_representative_email(submitter_email, submitter_name, submitter_organization, repo_cordra_id)
        return {"status": "okay", "repo_id": repo_cordra_id, "user_email": submitter_email}
    except Exception as ex:
        print(f"submit_application: send_label_application_as_repository_representative_email failed with exception: {ex}")
        raise HTTPException(status_code=500, detail=f"Sending email with application as repository representative failed.")


class ContactFormRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    submitter_name: str
    submitter_email: str
    subject: str
    message: str


# Submit aaplication to be approved as repository representative. This is triggered via the respective form for logged
# in users in the Label web frontend.
@app.post("/submit_contactform")
def submit_contactform(
    request: ContactFormRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:

    submitter_name = request.submitter_name
    submitter_email = request.submitter_email
    subject = request.subject
    message = request.message

    if not '@' in submitter_email:
        raise HTTPException(status_code=422, detail=f"Submitting contact form failed: Invalid 'submitter_email' email address.")

    if not DISABLE_AUTH_FOR_TESTING:
        check_user_logged_in(authorization=authorization)

    try:
        send_label_contact_email(subject, message, submitter_name, submitter_email)
        return {"status": "okay", "user_email": submitter_email}
    except Exception as ex:
        print(f"submit_contactform: send_label_contact_email failed with exception: {ex}")
        raise HTTPException(status_code=500, detail=f"Sending email with contact request failed.")


class SetSubasessmentCategoryRequest(BaseModel):
    """Model of the body of the query send to this endpoint. If a query does not match it, error 404 will be returned."""
    repoid: str
    assessmentid: str
    subassessmentuuid: str
    subassessmenttype: str   # one of 're3data', 'fuji', 'selfassessment'
    subassessmentcategory: str  # the new category to set, one of 'official', 'none'


@app.post("/set_subassessment_category")
def set_subassessment_official(
    request: SetSubasessmentCategoryRequest,
    authorization: Optional[str] = Header(None)
) -> Dict:
    """
    Endpoint to set the category of a subassessment in an assessment. You can use this to set it to official or none.
    Note that setting a subassessment to latest is not supported, that is handled by the function that saves a new subassessment to the database.
    @param repo_cordra_id: The repository cordra ID.
    @return Returns JSON with a status message, describing whether the storage of the selfassessment was accepted and worked out.
    """
    repo_id = request.repoid
    assessment_id = request.assessmentid
    subassessmentuuid = request.subassessmentuuid
    subassessmenttype = request.subassessmenttype
    subassessmentcategory = request.subassessmentcategory

    if not DISABLE_AUTH_FOR_TESTING:
        check_user_authorized_as_labelteammember(authorization=authorization)

    if not repo_exists_in_cordra(repo_id):
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: no such repo.")

    if not subassessmenttype in ['re3data', 'fuji', 'selfassessment']:
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: unknown subassessment type.")

    if subassessmentcategory == "latest":
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: setting to latest is not supported")

    if not subassessmentcategory in ['official', 'none']:
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: unknown subassessment category.")

    existed, assessment = ensure_repo_assessment_in_cordra(repo_id)

    # Some sanity checks.
    if not existed:
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: no such assessment.")
    if not assessment_id == assessment["id"]:
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: assessment ID mismatch.")

    # Retrieve the requested subassessment from the database to ensure it exists.
    our_subassessment = get_subassessment_with_uuid(assessment, SubAsmType(subassessmenttype), subassessmentuuid)
    if our_subassessment is None:
        raise HTTPException(status_code=422, detail=f"Setting subassessment category failed: no such subassessment.")

    if subassessmentcategory == "official":
        oldsubassessmenttype_was_official = assessment["content"]["results"]["achievedN4ELabel"][subassessmentcategory][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] != ""

    # Update the subassessment category in the assessment.
    if subassessmentcategory == "none":  # If it is set to none, we may need to remove its official status. Note that we do not remove the latest status, as this is considered a computed property based on the data, so messing with it manually and removing the status does not make any sense.
        oldsubassessmenttype_was_official = assessment["content"]["results"]["achievedN4ELabel"]["official"][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] == subassessmentuuid
        if oldsubassessmenttype_was_official:
            assessment["content"]["results"]["achievedN4ELabel"]["official"]["based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] = ""
    else:
        assessment["content"]["results"]["achievedN4ELabel"][subassessmentcategory][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] = subassessmentuuid

    # Re-compute the other 7 fields, (the 6 score fields like 'score_fuji', 'score_fuji_max_possible', ... and the overall 'result' field), based on the new data.
    official_subassessments = get_official_eval_instances(assessment["content"])
    achievedN4ELabel_official = compute_achievedN4ELabel_for_assessments(official_subassessments.get("re3data"), official_subassessments.get("fuji"), official_subassessments.get("selfassessment"))
    assessment["content"]["results"]["achievedN4ELabel"]["official"] = achievedN4ELabel_official
    # Note that the latest status is not set here, but computed based on the data. The latest status
    #  also cannot change if we set or unset an sub assessment to official or none, so we do not
    #  need to recompute it here.



    # Update the assessment in the database.
    try:
        update_cordra_object(assessment["content"], assessment_id, cordra_cfg=get_cordra_cfg())
        return {"status": "okay", "repo_id": repo_id, "assessment_id": assessment_id, "subassessment_uuid": subassessmentuuid, "subassessment_type": subassessmenttype, "subassessment_category": subassessmentcategory}
    except Exception as _:
        raise HTTPException(status_code=500, detail=f"Setting subassessment category failed: updating assessment in database failed.")


def set_subassessment_category_cmdline(repo_id : str, assessment_id: str, subassessmentuuid: str, subassessmenttype: str, subassessmentcategory: str):
    """
    Command line function to set the category of a subassessment in an assessment. You can use this to set it to official or none.
    TODO: Refactor this function to use the same code as the API endpoint set_subassessment_category.
    """
    if not repo_exists_in_cordra(repo_id):
        raise ValueError(f"Setting subassessment category failed: no such repo.")

    if not subassessmenttype in ['re3data', 'fuji', 'selfassessment']:
        raise ValueError(f"Setting subassessment category failed: unknown subassessment type.")

    if subassessmentcategory == "latest":
        raise ValueError(f"Setting subassessment category failed: setting to latest is not supported")

    if not subassessmentcategory in ['official', 'none']:
        raise ValueError(f"Setting subassessment category failed: unknown subassessment category.")

    existed, assessment = ensure_repo_assessment_in_cordra(repo_id)

    # Some sanity checks.
    if not existed:
        raise ValueError(f"Setting subassessment category failed: no such assessment.")
    if not assessment_id == assessment["id"]:
        raise ValueError(f"Setting subassessment category failed: assessment ID mismatch.")

    # Retrieve the requested subassessment from the database to ensure it exists.
    our_subassessment = get_subassessment_with_uuid(assessment, SubAsmType(subassessmenttype), subassessmentuuid)
    if our_subassessment is None:
        raise ValueError(f"Setting subassessment category failed: no such subassessment.")

    if subassessmentcategory == "official":
        oldsubassessmenttype_was_official = assessment["content"]["results"]["achievedN4ELabel"][subassessmentcategory][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] != ""

    # Update the subassessment category in the assessment.
    if subassessmentcategory == "none":  # If it is set to none, we may need to remove its official status. Note that we do not remove the latest status, as this is considered a computed property based on the data, so messing with it manually and removing the status does not make any sense.
        oldsubassessmenttype_was_official = assessment["content"]["results"]["achievedN4ELabel"]["official"][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] == subassessmentuuid
        if oldsubassessmenttype_was_official:
            assessment["content"]["results"]["achievedN4ELabel"]["official"]["based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] = ""
    else:
        assessment["content"]["results"]["achievedN4ELabel"][subassessmentcategory][f"based_on_subassessments"][f"{subassessmenttype}_subassessment_uuid"] = subassessmentuuid

    # Re-compute the other 7 fields, (the 6 score fields like 'score_fuji', 'score_fuji_max_possible', ... and the overall 'result' field), based on the new data.
    official_subassessments = get_official_eval_instances(assessment["content"])
    achievedN4ELabel_official = compute_achievedN4ELabel_for_assessments(official_subassessments.get("re3data"), official_subassessments.get("fuji"), official_subassessments.get("selfassessment"))
    assessment["content"]["results"]["achievedN4ELabel"]["official"] = achievedN4ELabel_official
    # Note that the latest status is not set here, but computed based on the data. The latest status
    #  also cannot change if we set or unset an sub assessment to official or none, so we do not
    #  need to recompute it here.



    # Update the assessment in the database.
    try:
        update_cordra_object(assessment["content"], assessment_id, cordra_cfg=get_cordra_cfg())
        return {"status": "okay", "repo_id": repo_id, "assessment_id": assessment_id, "subassessment_uuid": subassessmentuuid, "subassessment_type": subassessmenttype, "subassessment_category": subassessmentcategory}
    except Exception as _:
        raise ValueError(f"Setting subassessment category failed: updating assessment in database failed.")


@app.get("/assessments")
def get_assessments(
    authorization: Optional[str] = Header(None)
) -> Dict:
    cordra_result = get_all_assessments_from_cordra()

    is_label_team_admin = False

    if DISABLE_AUTH_FOR_TESTING:
        is_label_team_admin = True
    else:
        try:
            check_user_authorized_as_labelteammember(authorization=authorization)
            is_label_team_admin = True
        except Exception as _:
            pass

    assessments = cordra_result.get("results", [])

    for assessment in assessments:
        # iterate through the assessments list and for each assessment, remove the inner
        # fields
        repo_id = assessment["content"]["n4e:repository"]["handle"]
        is_authorized_for_repo = is_label_team_admin
        if not is_label_team_admin:
            try:
                check_user_authorized_to_request_for_repo(authorization, repo_id)
                is_authorized_for_repo = True
            except Exception as _:
                pass

            if not is_authorized_for_repo:
                # Hide the sub assessments from the user.
                assessment["content"]["results"]["re3data"] = []
                assessment["content"]["results"]["fuji"] = []
                assessment["content"]["results"]["selfassessment"] = []

    return cordra_result


badge_base_url = "https://img.shields.io/badge/"

def get_badge_url_for_achievedN4ELabel(achievedN4ELabel: Dict, badge_type="binary") -> str:

    if not badge_type in ["binary", "binary_with_score", "score"]:
        raise ValueError(f"get_badge_url_for_achievedN4ELabel: unknown badge_type '{badge_type}'")

    if badge_type == "binary":
        return _get_badge_url_binary_for_achievedN4ELabel(achievedN4ELabel)
    elif badge_type == "binary_with_score":
        return _get_badge_url_binary_with_score_for_achievedN4ELabel(achievedN4ELabel)
    else: # score
        return _get_badge_url_score_for_achievedN4ELabel(achievedN4ELabel)


def _get_badge_url_binary_for_achievedN4ELabel(achievedN4ELabel: Dict) -> str:

    badge_url_achieved = badge_base_url + "NFDI4Earth%20Label-Certified-green"
    badge_url_not_achieved = badge_base_url + "NFDI4Earth%20Label-Not%20Certified-blue"

    if achievedN4ELabel["result"]:
        return badge_url_achieved
    else:
        return badge_url_not_achieved


def _get_badge_url_binary_with_score_for_achievedN4ELabel(achievedN4ELabel: Dict) -> str:
    score_achieved = achievedN4ELabel["score_re3data"] + achievedN4ELabel["score_fuji"] + achievedN4ELabel["score_selfassessment"]
    score_max_possible = achievedN4ELabel["score_re3data_max_possible"] + achievedN4ELabel["score_fuji_max_possible"] + achievedN4ELabel["score_selfassessment_max_possible"]
    achieved_text = "Certified" if achievedN4ELabel["result"] else "Not%20Certified"
    score_text = "NFDI4Earth%20Label-" + achieved_text + "%20"+ str(score_achieved) + "%2F" + str(score_max_possible)
    color_text = "-green" if achievedN4ELabel["result"] else "-blue"
    badge_string = score_text + color_text
    badge_url = badge_base_url + badge_string
    return badge_url


def _get_badge_url_score_for_achievedN4ELabel(achievedN4ELabel: Dict) -> str:
    score_achieved = achievedN4ELabel["score_re3data"] + achievedN4ELabel["score_fuji"] + achievedN4ELabel["score_selfassessment"]
    score_max_possible = achievedN4ELabel["score_re3data_max_possible"] + achievedN4ELabel["score_fuji_max_possible"] + achievedN4ELabel["score_selfassessment_max_possible"]
    score_text = "NFDI4Earth%20Label-" + str(score_achieved) + "%2F" + str(score_max_possible)
    color_text = "-green" if achievedN4ELabel["result"] else "-blue"
    badge_string = score_text + color_text
    badge_url = badge_base_url + badge_string
    return badge_url


def _get_verify_url_for_repo(repo_name: str) -> str:
    return FRONTEND_VERIFY_URL + "/repository/" + repo_name


"""Return all information on the badge as JSON"""
@app.get("/repo_badge/{repo_name}")
def get_repo_badge(
    repo_name: str,
    badge_type: str = "binary_with_score",  # use like this for repo myrepo: localhost:8000/repo_badge/myrepo?badge_type=binary_with_score
    request: Optional[str] = Header(None)
) -> Dict:
    use_official = True
    response_json = _get_repo_badge(repo_name, badge_type, use_official)
    return response_json


"""Return the badge as SVG"""
@app.get("/badge/{repo_name}", response_class=HTMLResponse)
def get_badge(
    repo_name: str,
    badge_type: str = "binary_with_score",  # use like this for repo myrepo: localhost:8000/repo_badge/myrepo?badge_type=binary_with_score
    request: Optional[str] = Header(None)
) -> Dict:
    use_official = True
    response_json = _get_repo_badge(repo_name, badge_type, use_official)
    badge_url = response_json["badge_url"]
    svg = requests.get(badge_url).text
    return Response(content=svg, media_type="image/svg+xml", status_code=200)


def _get_repo_badge(repo_name: str, badge_type: str, use_official: bool) -> Dict:

    repoid = get_repo_id_for_name(repo_name)

    badge_error = badge_base_url + "NFDI4Earth%20Label-Invalid%20Request-lightgrey"
    has_label = False
    did_assessment_exist = False

    if repoid is None:
        status = "error"
        http_code = 404
        error_detail = "No repository with that name"
        badge_url = badge_error
    else:
        # Get the official assessment for the repository.
        if not badge_type in ["binary", "binary_with_score", "score"]:
            status = "error"
            http_code = 422
            error_detail = "Unknown badge type"
            badge_url = badge_error
        else:
            did_assessment_exist, assessment = ensure_repo_assessment_in_cordra(repoid)
            if not did_assessment_exist:
                status = "error"
                http_code = 404
                error_detail = "No assessment for that repository"
                badge_url = badge_error
            else:
                achievedN4ELabel = assessment["content"]["results"]["achievedN4ELabel"]["official"] if use_official else assessment["content"]["results"]["achievedN4ELabel"]["latest"]
                has_label = achievedN4ELabel["result"]
                status = "okay"
                http_code = 200
                error_detail = None
                badge_url = get_badge_url_for_achievedN4ELabel(achievedN4ELabel, badge_type)

    certification_status_static = "Certified" if has_label else "Not Certified"
    badge_html = "<img src='" + badge_url + "' alt='NFDI4EarthLabel " + certification_status_static + "'/>"
    badge_markdown = "![NFDI4Earth Label " + certification_status_static + "](" + badge_url + " \"NFDI4Earth Label " + certification_status_static+ "\")"
    badge_html_static = badge_html
    badge_markdown_static = badge_markdown
    if did_assessment_exist:
        verify_url = _get_verify_url_for_repo(repo_name=repo_name)
        badge_html_static = "<a href='" + verify_url + "'><img src='" + badge_url + "' /></a>"
        badge_markdown_static = "[![NFDI4EarthLabel " + certification_status_static + "](" + badge_url + ")](" + verify_url + ")"
        badge_url_dynamic = BACKEND_BASE_URL + "/badge/" + repo_name + "?badge_type=" + badge_type
        badge_html = "<a href='" + verify_url + "'><img src='" + badge_url_dynamic + "' /></a>"
        badge_markdown = "[![NFDI4EarthLabel](" + badge_url_dynamic + ")](" + verify_url + ")"

    full_json = {
        "status": status,
        "status_code": http_code,
        "error_detail": error_detail,
        "badge_url": badge_url,
        "badge_html" : badge_html,
        "badge_html_static" : badge_html_static,
        "badge_markdown": badge_markdown,   # ;arkdown with included SVG images only works in some markdown renderers, like GitHub.
        "badge_markdown_static": badge_markdown_static,  # Markdown with included static image links, works in all markdown renderers.
        "label_achieved": str(has_label),
    }

    return full_json


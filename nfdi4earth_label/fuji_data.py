#!/usr/bin/env python

import json
import requests
import random
from datetime import datetime
from requests.auth import HTTPBasicAuth
from .custom_processing import custom_analyze_records
import pandas as pd
from sickle import Sickle
import logging
from dataclasses import dataclass
from .fuji_eval_status import FujiEvalStatus
from .datamodel import get_singlemetric_score_object
from .settings import get_settings

settings = get_settings()
FUJI_API_URL = settings.get("FUJI_API_URL")
FUJI_USER = settings.get("FUJI_USER")
FUJI_PASSWORD = settings.get("FUJI_PASSWORD")


from .common import get_datetime_now_in_cordra_format, get_eval_default, generate_uuid, get_evaluation_version, evaluated_by_default_value, get_eval_default_fuji_entry, metrics
from typing import Dict, Union, Tuple
logger = logging.getLogger(__name__)

# NOTE: requires F-UJI to be running (currently under http://localhost:1071/fuji/api/v1/evaluate)

# test repos:
pangaea = "https://ws.pangaea.de/oai/provider"
oceanrep = "https://oceanrep.geomar.de/cgi/oai2"
wdcc = "http://c3grid1.dkrz.de:8080/oai/oaisearch.do"  # unknown error with sickle.next() function
gfz = "https://doidb.wdc-terra.org//oaip/oai"




def extract_results(results: dict) -> dict:
    """
    Extract the results for all metrics from the F-UJI response 'results' section.
    """
    result_details = dict()
    for result in results:
        result_details[result["metric_identifier"]] = (
            100.0 if result["test_status"] == "pass" else result["score"]["earned"] / result["score"]["total"] * 100.0
        )
    return result_details


@dataclass
class FujiEvalResult:
    """Class for storing F-UJI evaluation result."""

    status_code: int = FujiEvalStatus.NO_EVAL_DONE_NO_API
    fuji_overview: pd.DataFrame = (
        None  # Overview section of the F-UJI API response, the mean score over all sampled items for the metrics.
    )
    fuji_details: pd.DataFrame = None  # Full F-UJI API response for the sampled items.
    custom_processing_results: dict = (
        None  # Results from the custom processing of the records, e.g., whether they contain ORCIDs or RORs.
    )
    fuji_json_details : dict = None

    @property
    def status_string(self) -> str:
        return FujiEvalStatus.descriptions[self.status_code]

    @property
    def was_successful(self) -> bool:
        return self.status_code == FujiEvalStatus.SUCCESS


def get_fuji_cfg():
    """Get FUJI service login information."""
    return {"user": FUJI_USER, "password": FUJI_PASSWORD, "FUJI_API_URL": FUJI_API_URL}


def compute_repo_fuji_eval_result(
    repo_url: str, fuji_cfg: Dict = get_fuji_cfg(), repo_name: Union[str, None] = None, selection_n: int = 3
) -> tuple[dict, str]:
    """
    Compute FUJI assessment for a repo.
    @return Tuple of: (1) Only the inner fuji sub assessment and (2) status string of the Fuji assessment.
    """
    eval_result = get_eval_default()
    fuji_eval_result: FujiEvalResult = fuji_eval(
        repo_url=repo_url, fuji_cfg=fuji_cfg, repo_name=repo_name, selection_n=selection_n
    )
    eval_result: Dict = add_fuji_eval_result_to_eval_result(eval_result, fuji_eval_result)
    return eval_result["results"]["fuji"][0], fuji_eval_result.status_string, fuji_eval_result.fuji_json_details


def is_fuji_service_running(fuji_cfg: Dict = get_fuji_cfg()) -> Tuple[bool, str]:
    api_url_pangaea = "https://ws.pangaea.de/oai/provider"
    some_dataset_from_pagaea_doi = "https://doi.pangaea.de/10.1594/PANGAEA.142389"
    try:
        _ = requests.post(
            fuji_cfg.get("FUJI_API_URL"),
            headers={
                "accept": "application/json",
                "content-type": "application/json",
            },
            data=json.dumps(
                {
                    "metadata_service_endpoint": api_url_pangaea,
                    "metadata_service_type": "oai_pmh",
                    "object_identifier": some_dataset_from_pagaea_doi,
                    "test_debug": False,
                    "use_datacite": True,
                }
            ),
            auth=HTTPBasicAuth(fuji_cfg["user"], fuji_cfg["password"]),
        )
        return (True, "")
    except Exception as ex:
        if "[Errno 111] Connection refused" in str(ex):
            return False, str(ex)
        else:  # another error, maybe authentication issue or whatever. What do we return now?
            return False, str(ex)


def fuji_eval(repo_url: str, fuji_cfg: Dict = get_fuji_cfg(), repo_name=None, selection_n=3, check_fuji_first=True) -> FujiEvalResult:
    """
    Use a F-UJI instance to evaluate a repository. Typically the F-UJI server is run locally. This can be done by cloning the F-UJI repository and running the docker-compose file, or using standalone Python installation. You could also use the public remote instance though.
    Note that we sample the repository several times (with different items) to get a more robust result, and the scores returned are the average of the individual scores.

    @repoURL The OAIPMH API URL for a repository supporting OAI-PMH. You can get the URL from the re3data information in the knowledge graph, if it is available.
    @fuji_cfg dict with user and password for the F-UJI instance.
    @repo_name Optional name of the repository. If not given, the repo_url is used.
    @selection_n Number of items to sample from the repository. Default is 3, but we recommend to use more for the full evaluation.
    @param check_fuji_first: whether to check for running F-UJI service first, before trying to connect to repo via sickle. The adcantage of checking first and aborting if F-UJI is not running is that you save a lot of time per repo in case F-UJI is down. The disadvantage is that you get less information about the repo, all the info you get is that F-UJI is down. However, a scan with F-UJI down makes little sense and having F-UJI running is in your reponsibility, so the default is to fail fast if F-UJI is down.
    @return tuple of (status_code, pd.dataframe with results, pd.dataframe with result details). See `fuji_eval_status_codes` for the code meanings. The dataframe contains the F-UJI results (mean score over all `selection_n` randomly sampled items) in the repository, see the F-UJI documentation for details. E.g., https://www.f-uji.net/index.php?action=methods
    """

    fuji_json_details = FujiEvalStatus.getDetailJsonTemplate()
    fuji_json_details[FujiEvalStatus.FoundApiUrl]["result"] = True

    if check_fuji_first:
        fuji_running, _ =  is_fuji_service_running(fuji_cfg)
        if not fuji_running:
            return FujiEvalResult(FujiEvalStatus.FAILED_FUJI_CONNECT, None, None, None, fuji_json_details)

    if repo_name is None:
        repo_name = repo_url
    # number of items to sample from the available items in the reop. Planned for full set is 30.
    logger.info(f"Starting F-UJI evaluation of repo '{repo_url}' ({repo_name}) using {selection_n} sample items.")

    # create iterator
    sickle = Sickle(repo_url)

    # Check if repo_url is valid OAI-PMH endpoint by running IDENTIFY OAIPMH verb first.
    do_try_identify = False
    if do_try_identify:
        try:
            identify_response = sickle.Identify()
            fuji_json_details[FujiEvalStatus.FujiConnectSuccessful]["result"] = True
            logger.debug(f" -Repo {repo_name} Identify response:")
            for key, value in identify_response.__dict__.items():
                if key != "raw":
                    logger.debug(f"  * {key}: {value}")
        except Exception as e:
            logger.error(f" * Error during Identify request: Unable to identify repo {repo_name}: {str(e)}.")
            return FujiEvalResult(FujiEvalStatus.FAILED_IDENTIFY, None, None, None, fuji_json_details)

    try:
        identifiers = sickle.ListIdentifiers(metadataPrefix="oai_dc", ignore_deleted=True)
        fuji_json_details[FujiEvalStatus.ListIdentifiersSuccessful]["result"] = True
        # Maybe try ListRecords to retrieve "Resource Identifier" field from response, to pass to F-UJI
        # x = sickle.ListRecords(metadataPrefix="oai_dc")
        # https://ws.pangaea.de/oai/provider?verb=ListRecords&metadataPrefix=oai_dc
    except Exception as e:
        logger.error(
            f" * Error during ListIdentifiers request: Unable to list record identifiers for repo {repo_name}: {str(e)}."
        )
        return FujiEvalResult(FujiEvalStatus.FAILED_LIST_IDENTIFIERS, None, None, None, fuji_json_details)

    do_list_records = False  # This takes some time and puts quite a bit of burden on the repo, so we only do it if we have to. We may need to do itto get the identifier through, which does not seem to be part of the ListIdentifiers response.
    if do_list_records:
        try:
            records = sickle.ListRecords(metadataPrefix="oai_dc", ignore_deleted=True)
            fuji_json_details[FujiEvalStatus.ListRecordsSuccessful]["result"] = True
            for record in records:
                logger.debug(f" -Repo {repo_name} Record response:")
                for key, value in record.__dict__.items():  # This potentially prints A LOT of information.
                    if key != "raw":
                        logger.debug(f"  * record {key}: {value}")
        except Exception as e:
            logger.error(f" * Error during ListRecords request: Unable to list records for repo {repo_name}: {str(e)}.")
            return FujiEvalResult(FujiEvalStatus.FAILED_LIST_RECORDS, None, None, None, fuji_json_details)

    # Extract sample set of dataset IDs.
    try:
        list_ids = [l for l in identifiers]
        num_identifiers = len(list_ids)
        selection = random.sample(list_ids, selection_n)
        fuji_json_details[FujiEvalStatus.ListRecordsNotEmpty]["result"] = True
    except Exception as e:
        logger.error(
            f" * Error during selection of {selection_n} randomly sampled items (out of {num_identifiers} available ones) for repo {repo_name}: {str(e)}."
        )
        return FujiEvalResult(FujiEvalStatus.TOO_FEW_RECORDS, None, None, None, fuji_json_details)

    # Retrieve full records for sample set.
    sample_items = list()
    try:
        for item in selection:
            item_id = item.identifier
            item_record = sickle.GetRecord(metadataPrefix="oai_dc", identifier=item_id)
            fuji_json_details[FujiEvalStatus.AccessOneRecordSuccessful]["result"] = True
            sample_items.append(item_record)
    except Exception as e:
        logger.error(
            f" * Error during GetRecords request: Unable to retrieve record '{item_id}' for repo {repo_name}: {str(e)}."
        )
        return FujiEvalResult(FujiEvalStatus.FAILED_GET_RECORD, None, None, None, fuji_json_details)
    fuji_json_details[FujiEvalStatus.AccessAllRequestedRecordsSuccessful]["result"] = True

    custom_processing_results = {
        "contains_orcid": False,
        "contains_ror": False,
    }

    try:
        custom_processing_results = custom_analyze_records(sample_items)
    except Exception as e:
        logger.error(f" * Error during custom processing of records for repo {repo_name}: {str(e)}.")
        return FujiEvalResult(FujiEvalStatus.FAILED_CUSTOM_PROCESSING, None, None, None, fuji_json_details)

    start = datetime.now()

    list_of_results = (
        []
    )  # overview in broad categories, list of dicts (one dict per item, e.g., "F2": 100, "F3": 50, ...)
    result_details = (
        []
    )  # detailed results for each item, list of dicts (one dict per item, e.g., "FsF-A1-02M-1": 1, ..)

    fuji_api_url = fuji_cfg.get("FUJI_API_URL", FUJI_API_URL)

    try:
        for item_idx, item in enumerate(sample_items):
            # create list of all possible identifiers for dataset
            id_poss = item.metadata["identifier"]
            id_poss.append(item.header.identifier)

            for idp in id_poss:
                if idp.startswith("http"):
                    logger.info(
                        f" * Evaluating sampled item {item_idx+1} of {len(sample_items)} with identifier {idp}..."
                    )
                    try:
                        response = requests.post(
                            fuji_api_url,
                            headers={
                                "accept": "application/json",
                                "content-type": "application/json",
                            },
                            data=json.dumps(
                                {
                                    "metadata_service_endpoint": repo_url,
                                    "metadata_service_type": "oai_pmh",
                                    "object_identifier": idp,
                                    "test_debug": False,
                                    "use_datacite": True,
                                }
                            ),
                            auth=HTTPBasicAuth(fuji_cfg["user"], fuji_cfg["password"]),
                        )
                        fuji_json_details[FujiEvalStatus.FujiConnectSuccessful]["result"] = True
                        fuji_json_details[FujiEvalStatus.FujiScanSuccessOneRecord]["result"] = True
                    except Exception as ex:
                        if "[Errno 111] Connection refused" in str(ex):
                            raise Exception("Failed to connect to F-UJI service: it may not be running").with_traceback(ex.__traceback__)


                    # Check respose HTTP status code / HTTP errors
                    response.raise_for_status()

                    fuji_response = response.json()

                    list_of_results.append(fuji_response["summary"]["score_percent"])
                    result_details.append(extract_results(fuji_response["results"]))
                    break
                else:
                    logger.info(" * Skipping item with invalid identifier format: " + idp)
            fuji_json_details[FujiEvalStatus.FujiScanSuccessAllRecords]["result"] = True

        logger.info(
            f"Finished F-UJI evaluation of repo '{repo_url}' using {selection_n} sample items, duration: {(datetime.now() - start)}."
        )
    except Exception as e:
        logger.error(
            f" * Error during F-UJI request: Unable to evaluate record '{id_poss}' for repo {repo_name}: {str(e)}."
        )
        if "Failed to connect to F-UJI service" in str(e):
            return FujiEvalResult(FujiEvalStatus.FAILED_FUJI_CONNECT, None, None, None, fuji_json_details)
        else:
            return FujiEvalResult(FujiEvalStatus.FAILED_FUJI_FOR_RECORD, None, None, None, fuji_json_details)

    df_fuji_overview = pd.DataFrame(list_of_results)
    df_fuji_overview = df_fuji_overview.mean(axis=0)  # compute column-wise mean (over the selection_n sample items)

    df_fuji_details = pd.DataFrame(result_details)
    df_fuji_details = df_fuji_details.mean(axis=0)  # compute column-wise mean (over the selection_n sample items)

    return FujiEvalResult(FujiEvalStatus.SUCCESS, df_fuji_overview, df_fuji_details, custom_processing_results, fuji_json_details)




def add_fuji_eval_result_to_eval_result(eval_result: dict, fuji_eval_result_in: FujiEvalResult) -> dict:
    """
    Add the F-UJI evaluation result to the existing evaluation result.

    This turns the data from the evaluation pandas dataframe into a dict and adds it to the existing evaluation result dict.

    @param eval_result The existing evaluation result dict.
    @param fuji_eval_result The F-UJI evaluation result of type `FujiEvalResult`.
    """
    df_overview = fuji_eval_result_in.fuji_overview
    df_details = fuji_eval_result_in.fuji_details
    status = fuji_eval_result_in.status_code

    logger.info(
        f" * Adding F-UJI evaluation result (status: {status}, {fuji_eval_result_in.status_string}) to evaluation result."
    )


    fuji_eval_time = get_datetime_now_in_cordra_format()

    fuji_eval_technical_success = bool(status == FujiEvalStatus.SUCCESS)

    schemanames_to_fujifieldnames_overview = (
        {  # see Excel sheet in sharepoint and https://www.f-uji.net/index.php?action=methods
            "hasDescriptiveMetadata": "F2",
            "metadataIncludesIdentifier": "F3",
            "metadataLinks": "I3",
            "includesLicense": "R1.1",
            "includesProvenance": "R1.2",
        }
    )

    schemanames_to_descriptions_overview = {
        "hasDescriptiveMetadata": "Dataset has descriptive metadata",
        "metadataIncludesIdentifier": "Metadata includes dataset identifier",
        "metadataLinks": "Metadata includes links between the data and its related entities",
        "includesLicense": "Metadata includes license information",
        "includesProvenance": "Metadata includes provenance information",
    }

    fuji_results_instance = get_eval_default_fuji_entry()
    fuji_results_instance["uuid"] = generate_uuid()
    fuji_results_instance["evaluated_at"] = fuji_eval_time
    fuji_results_instance["evaluated_by"] = evaluated_by_default_value
    fuji_results_instance["evaluation_version"] = get_evaluation_version()
    fuji_results_instance["scan"] = dict()
    fuji_results_instance["scan"]["fuji_scan_datetime"] = fuji_eval_time
    fuji_results_instance["scan"]["fuji_scan_technical_success"] = fuji_eval_technical_success
    fuji_results_instance["scan"]["fuji_scan_technical_status_message"] = fuji_eval_result_in.status_string

    for metric_name, fuji_columnname in schemanames_to_fujifieldnames_overview.items():
        logger.debug(
            f"   - Adding F-UJI evaluation result of metric {metric_name} from F-UJI result column {fuji_columnname} to evaluation result."
        )
        fuji_results_instance["metrics"][metric_name] = dict()
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"] = get_singlemetric_score_object()
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = 0
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_max_possible"] = metrics["fuji"][metric_name]["max_score_possible"]
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_not_achieved_suggestion"] = metrics["fuji"][metric_name]["suggestion"]
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["required_for_label"] = metrics["fuji"][metric_name]["required_for_label"]
        fuji_results_instance["metrics"][metric_name]["reason"] = dict()
        fuji_results_instance["metrics"][metric_name]["description_short"] = schemanames_to_descriptions_overview[metric_name]
        if fuji_eval_technical_success and type(df_overview) == pd.Series and fuji_columnname in df_overview.index:
            fuji_results_instance["metrics"][metric_name]["result"] = (
                bool(df_overview[fuji_columnname] >= 50.0) if fuji_eval_technical_success else False
            )
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score"] = (
                df_overview[fuji_columnname] if fuji_eval_technical_success else 0.0
            )  # score for this schema name / fuji field
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score_field_name"] = fuji_columnname
            fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = metrics["fuji"][metric_name]["max_score_possible"] if fuji_results_instance["metrics"][metric_name]["result"] else 0
        else:
            if fuji_eval_technical_success:
                logger.warning(
                    f"   - F-UJI result column {fuji_columnname} not found in F-UJI result overview dataframe."
                )
            fuji_results_instance["metrics"][metric_name]["result"] = False
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score"] = 0.0
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score_field_name"] = fuji_columnname
            fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = 0

    # Now get the stuff we cannot get from the overview dataframe from the details.

    schemanames_to_fujifieldnames_details = {  # see Excel sheet in sharepoint and https://www.f-uji.net/index.php?action=methods
        "metadataRepresentation": "FsF-I1-01M",
        # "semanticResources": "FsF-I1-02M", # TODO: why is this not part of the response? it should according to FUJI documentation. But it is also missing on the offical FUJI instance at https://www.f-uji.net/?action=test
        "specifiesContent": "FsF-R1-01MD",
    }

    schemanames_to_descriptions_details = {
        "metadataRepresentation": "Metadata is represented using a formal knowledge representation language",
        "specifiesContent": "Metadata specifies the content of the data",
    }

    for metric_name, fuji_columnname in schemanames_to_fujifieldnames_details.items():
        logger.debug(
            f"   - Adding F-UJI evaluation result of schema entry {metric_name} from F-UJI result column {fuji_columnname} to evaluation result."
        )
        fuji_results_instance["metrics"][metric_name] = dict()
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"] = get_singlemetric_score_object()
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = 0
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_max_possible"] = metrics["fuji"][metric_name]["max_score_possible"]
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_not_achieved_suggestion"] = metrics["fuji"][metric_name]["suggestion"]
        fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["required_for_label"] = metrics["fuji"][metric_name]["required_for_label"]


        fuji_results_instance["metrics"][metric_name]["reason"] = dict()
        fuji_results_instance["metrics"][metric_name]["description_short"] = schemanames_to_descriptions_details[metric_name]
        # if fuji_eval_technical_success and type(df_overview) == pd.DataFrame and fuji_columnname in df_details.columns:
        # using currently fuji tool v3.2.0
        if fuji_eval_technical_success and type(df_details) == pd.Series and fuji_columnname in df_details.index:
            fuji_results_instance["metrics"][metric_name]["result"] = (
                bool(df_details[fuji_columnname] >= 1.0) if fuji_eval_technical_success else False
            )
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score"] = (
                df_details[fuji_columnname] if fuji_eval_technical_success else 0.0
            )  # score for this schema name / fuji field
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score_field_name"] = fuji_columnname
            fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = metrics["fuji"][metric_name]["max_score_possible"] if fuji_results_instance["metrics"][metric_name]["result"] else 0
        else:
            if fuji_eval_technical_success:
                logger.warning(
                    f"   - F-UJI result column {fuji_columnname} not found in F-UJI result details dataframe."
                )
            fuji_results_instance["metrics"][metric_name]["result"] = False
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score"] = 0.0
            fuji_results_instance["metrics"][metric_name]["reason"]["fuji_score_field_name"] = fuji_columnname
            fuji_results_instance["metrics"][metric_name]["singlemetricscore"]["score_achieved"] = 0

    if not "fuji" in eval_result["results"].keys():
        eval_result["results"]["fuji"] = list()

    eval_result["results"]["fuji"].append(fuji_results_instance)
    return eval_result



#!/usr/bin/env python
#
# Functions for storing the results of the repository evaluation back to the database.

import json
import logging
import rdflib
from .query_metadata import get_metadata, get_ess_subject_areas, N4E, NFDICO
from rdflib.namespace import DCTERMS, DCAT
from .cordra_util import (
    get_cordra_cfg,
    find_cordra_object_by_id,
    get_datetime_now_in_cordra_format,
    upload_repo_eval_result,
    get_unique_repo_name_for_id,
    create_test_repo,
)
from typing import Union, Dict
from .common import AssessmentPart, get_eval_default, get_evaluation_version, generate_uuid, get_eval_default_re3data_entry
from .webapi import WebApi

logger = logging.getLogger(__name__)


def store_re3data_repo_eval_result(
    repo_id: str,
    repo_name: str,
    cordra_cfg: Dict = get_cordra_cfg(),
    create_repo_if_not_exists: bool = False,
    label_eval_result=None,
) -> None:
    """
    Compute and save re3data repo assessment data back to the database.

    Currently we only store things which can be queried from the database anyways, which may seem to
    make limited sense. However, in the future we will add more data, including F-UJI output, so this
    is justified.

    Parameters
    ----------
    repo_id: the identifier for the repository that should be evaluated. Note that the ID must be an ID in the public 4Earth knowledge hub Cordra instance, NOT from the Cordra on your local machine. Go to https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api to find valid IDs.
    repo_name: the repository name, used for logging only. Should be the official name from re3data.
    cordra_cfg: dict with CORDRA configuration, see variables.env in knowledge-hub-backend-setup repo
    create_repo_if_not_exists: if True, the repository is created in the database if it does not exist yet. This is useful for testing.
    label_eval_result: optional existing re3data eval result. If left at the default value None, it is queried via SPARQL from the Knowledhe Hub and computed.
    """
    if label_eval_result is None:
        label_eval_result = compute_repo_re3data_eval_result(repo_id, repo_name)
    relevant_parts = [AssessmentPart.RE3DATA.value]
    logger.info(f"Storing data for repo '{repo_name}' with ID '{repo_id}'.")

    obj = find_cordra_object_by_id(object_type="Repository", object_id=repo_id, cordra_cfg=cordra_cfg)
    if obj is None:
        if create_repo_if_not_exists:
            logger.warning(
                f"Creating repository '{repo_name}' in cordra as no such repo was found and 'create_repo_if_not_exists' is true."
            )
            local_repo_id = create_test_repo(cordra_cfg, repo_id, repo_name)
            label_eval_result["n4e:repository"] = local_repo_id
        else:
            logger.warning(
                f"No repo named '{repo_name}' found for in cordra, trying to upload eval results without 'n4e:repository' field."
            )

    upload_repo_eval_result(label_eval_result, relevant_parts, cordra_cfg=cordra_cfg)


def compute_repo_re3data_eval_result(repo_id: str, repo_name: Union[str, None] = None) -> Dict:
    """
    Compute re3data repo assessment. Retrieves metadata from the KH Fuseki database via SPARQL and computes the evaluation result.

    Parameters
    ----------
    @param repo_id: the identifier for the repository that should be evaluated. Note that the ID must be an ID in the public 4Earth knowledge hub Cordra instance, NOT from the Cordra on your local machine. Go to https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api to find valid IDs.
    @param repo_name: the repository name, used for logging only. Should be the official name from re3data. Can be set to None to auto-determine it from cordra.
    @return a full eval result. Note that the single instance of a re3data evaluation that is relevant is guaranteed to be the first entry in the ```results => re3data``` array.
    """
    if repo_name is None:
        repo_name = get_unique_repo_name_for_id(repo_id)
    if repo_name is None:
        raise ValueError("Could not determine repo name for repo.")

    repo_metadata_graph = get_metadata(repo_id)  # SPARQL query to get metadata about the repository.

    if not (None, DCAT.endpointURL, None) in repo_metadata_graph:
        logger.debug(f"This graph does not contain triples about API endpoint URLs (repo={repo_name}).")

    for s, p, o in repo_metadata_graph.triples((None, DCAT.endpointURL, None)):
        logger.debug(f" * API access triple: {s}, {p}, {o}")

    logger.debug(f"Computing eval data for repo '{repo_name}' with ID '{repo_id}'.")
    logger.debug(f"Repo metadata: {str(repo_metadata_graph)}")
    label_eval_result = _construct_label_re3data_eval_result(repo_metadata_graph, repo_id, repo_name)
    return label_eval_result


def _construct_label_re3data_eval_result(
    repo_metadata: rdflib.graph.ConjunctiveGraph, repo_id: str, repo_name: str
) -> Dict:
    """
    Construct the label re3data eval result from the repo metadata, i.e., the result of the SPAQRL query.

    :param repo_metadata: the SPARQL query result
    :param repo_id: the repository identifier
    :return: a label eval result dictionary, with proper schema (see JSON schema in knowledge-hub-backend-setup repo). The dictionary
    is guaranteed to have a single entry in the ```results => re3data``` array. Use this entry.
    """

    logger.debug(f"Received SPARQL query result: '{repo_metadata}' of type '{type(repo_metadata)}'.")

    # deep clone the default eval object
    eval_result = json.loads(json.dumps(get_eval_default()))
    eval_result["evaluated_at"] = get_datetime_now_in_cordra_format()
    eval_result["evaluation_version"] = get_evaluation_version()
    eval_result["n4e:repository"]["name"] = repo_name
    eval_result["n4e:repository"]["handle"] = repo_id
    eval_result["results"]["re3data"] = []

    re3data_res = get_eval_default_re3data_entry()
    re3data_res["evaluated_at"] = get_datetime_now_in_cordra_format()
    re3data_res = _determine_capabilities_F(repo_metadata, re3data_res, repo_name)
    re3data_res = _determine_capabilities_A(repo_metadata, re3data_res, repo_name)
    re3data_res = _determine_capabilities_I(repo_metadata, re3data_res, repo_name)
    re3data_res = _determine_capabilities_R(repo_metadata, re3data_res, repo_name)
    re3data_res["uuid"] = generate_uuid()

    eval_result["results"]["re3data"].append(re3data_res)

    return eval_result


def _determine_capabilities_F(
    repo_metadata: rdflib.graph.ConjunctiveGraph, eval_result_re3data_entry: dict = {}, repo_name: str = "unknown"
) -> dict:
    """
    Set the metrics_f part (Findability) in the given dict and return it.
    @param repo_metadata: repo information in rdflib graph format, queried from SPARQL endpoint
    @param eval_result_re3data_entry: a single instance of a member of the ```results => re3data``` array part of a full eval object.
    @param repo_name: string, used for logging only
    """

    logger.debug(f"Checking repo {repo_name} capabilities for section F.")

    for key in ["assignsPersistentIds", "assignsUniqueIds"]:
        if not key in eval_result_re3data_entry["metrics"].keys():
            raise ValueError(f"Key '{key}' not found in eval result dict.")

    # n4e-assignsUniqueIds and n4e-assignsPersistentIds
    eval_result_re3data_entry["metrics"]["assignsPersistentIds"]["result"] = False
    eval_result_re3data_entry["metrics"]["assignsPersistentIds"]["reason"]["supports"] = list()
    eval_result_re3data_entry["metrics"]["assignsUniqueIds"]["reason"]["supports"] = list()
    eval_result_re3data_entry["metrics"]["assignsUniqueIds"]["result"] = False

    persistent_ids_ref = ["http://purl.org/spar/datacite/doi", "http://purl.org/spar/datacite/handle"]

    unique_ids_ref = [
        "http://purl.org/spar/datacite/isbn",
        "http://purl.org/spar/datacite/url",
        "http://purl.org/spar/datacite/urn",
    ]

    # Query the rdflib graph:
    # https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph.objects
    for triplet in repo_metadata.objects(None, N4E.assignsIdentifierScheme):
        triplet = str(triplet)
        logger.debug(f"Repo {repo_name} assigns identifier scheme: {triplet}")

        if triplet in unique_ids_ref:
            capability = eval_result_re3data_entry["metrics"]["assignsUniqueIds"]
            capability["result"] = True
            capability["reason"]["supports"].append(triplet)
        elif triplet in persistent_ids_ref:
            capability = eval_result_re3data_entry["metrics"]["assignsUniqueIds"]
            capability["result"] = True
            capability["reason"]["supports"].append(triplet)
            capability = eval_result_re3data_entry["metrics"]["assignsPersistentIds"]
            capability["result"] = True
            capability["reason"]["supports"].append(triplet)
        else:
            logger.debug(f"Identifier scheme {triplet} supported by repo {repo_name} not in reference list.")

    eval_result_re3data_entry["metrics"]["assignsPersistentIds"]["reason"]["supports"] = list(
        set(eval_result_re3data_entry["metrics"]["assignsPersistentIds"]["reason"]["supports"])
    )
    eval_result_re3data_entry["metrics"]["assignsUniqueIds"]["reason"]["supports"] = list(
        set(eval_result_re3data_entry["metrics"]["assignsUniqueIds"]["reason"]["supports"])
    )

    return eval_result_re3data_entry


def _determine_capabilities_A(
    repo_metadata: rdflib.graph.ConjunctiveGraph, eval_result_re3data_entry: dict = {}, repo_name: str = "unknown"
) -> dict:
    """
    Set the metrics_a part (Accessability) in the given dict and return it.
    @param repo_metadata: repo information in rdflib graph format, queried from SPARQL endpoint
    @param eval_result_re3data_entry: a single instance of a member of the ```results => re3data``` array part of a full eval object.
    @param repo_name: string, used for logging only
    """
    logger.debug(f"Checking repo {repo_name} capabilities for section A.")

    for key in ["providesAnyAPI", "supportsApisHarvestable"]:
        if not key in eval_result_re3data_entry["metrics"].keys():
            raise ValueError(f"Key '{key}' not found in eval result dict.")

    api_standard_manual = [WebApi.API_REST.value, WebApi.API_FTP.value]
    api_standard_harvestable = [WebApi.API_NETCDF.value, WebApi.API_OAI_PMH.value, WebApi.API_OGC_CSW.value]

    repo_api_standards = []
    repo_api_endpoint_urls = []  # Collect all API endpoint URLs for later use.
    for api in repo_metadata.objects(None, N4E.hasAPI):
        for api_standard in repo_metadata.objects(api, DCTERMS.conformsTo):
            repo_api_standards.append(str(api_standard))
        for api_endpoint_url in repo_metadata.objects(api, DCAT.endpointURL):
            repo_api_endpoint_urls.append(str(api_endpoint_url))

    for idx, api in enumerate(repo_api_standards):
        if api in api_standard_manual:
            capability = eval_result_re3data_entry["metrics"]["providesAnyAPI"]
            capability["result"] = True
            capability["reason"]["supports"].append(api)
            capability["reason"]["endpoints"].append(repo_api_endpoint_urls[idx])
            capability["singlemetricscore"]["score_achieved"] = capability["singlemetricscore"]["score_max_possible"]

        elif api in api_standard_harvestable:
            capability = eval_result_re3data_entry["metrics"]["providesAnyAPI"]
            capability["result"] = True
            capability["reason"]["supports"].append(api)
            capability["reason"]["endpoints"].append(repo_api_endpoint_urls[idx])
            capability = eval_result_re3data_entry["metrics"]["supportsApisHarvestable"]
            capability["result"] = True
            capability["reason"]["supports"].append(api)
            capability["reason"]["endpoints"].append(repo_api_endpoint_urls[idx])
            capability["singlemetricscore"]["score_achieved"] = capability["singlemetricscore"]["score_max_possible"]
        else:
            logger.debug(f"API {api} supported by repo {repo_name} not in reference list, ignored during evaluation.")

    return eval_result_re3data_entry


def _determine_capabilities_I(
    repo_metadata: rdflib.graph.ConjunctiveGraph, eval_result_re3data_entry: dict = {}, repo_name: str = "unknown"
) -> dict:
    """
    Set the metrics_i part (Interoperability) in the given dict and return it.
    @param repo_metadata: repo information in rdflib graph format, queried from SPARQL endpoint
    @param eval_result_re3data_entry: a single instance of a member of the ```results => re3data``` array part of a full eval object.
    @param repo_name: string, used for logging only
    """
    # these capabilities are evaluated based on F-UJI, we do not set anything here at this time.
    return eval_result_re3data_entry


def _determine_capabilities_R(
    repo_metadata: rdflib.graph.ConjunctiveGraph, eval_result_re3data_entry: dict = {}, repo_name: str = "unknown"
) -> dict:
    """
    Set the metrics_r part (Reuseability) in the given dict and return it.
    @param repo_metadata: repo information in rdflib graph format, queried from SPARQL endpoint
    @param eval_result_re3data_entry: a single instance of a member of the ```results => re3data``` array part of a full eval object.
    @param repo_name: string, used for logging only
    """
    # supportsCrossDomainStandard and supportsSpecificESSStandard
    for key in ["supportsCrossDomainStandard", "supportsSpecificESSStandard"]:
        if not key in eval_result_re3data_entry["metrics"].keys():
            raise ValueError(f"Key '{key}' not found in eval result dict.")

    eval_result_re3data_entry["metrics"]["supportsCrossDomainStandard"]["reason"]["supports"] = list()
    eval_result_re3data_entry["metrics"]["supportsSpecificESSStandard"]["reason"]["supports"] = list()

    cross_standards = [
        "Dublin Core", "Schema.org", "DataCite Metadata Schema", "DCAT (Data Catalog Vocabulary)"
    ]
    scientific_domains_ess = get_ess_subject_areas()
    for metadata_standard in repo_metadata.objects(None, N4E.supportsMetadataStandard):
        # supportsCrossDomainStandard
        supports_cross_domain = False
        ms_title = list(
            repo_metadata.objects(metadata_standard, DCTERMS.title)
        )
        for title in ms_title:
            if str(title) in cross_standards:
                supports_cross_domain = True
                break
        if supports_cross_domain:
            capability = eval_result_re3data_entry["metrics"]["supportsCrossDomainStandard"]
            capability["result"] = True
            capability["reason"]["supports"].append(str(ms_title[0]) if len(ms_title)==1 else str(ms_title))
            capability["singlemetricscore"]["score_achieved"] = capability["singlemetricscore"]["score_max_possible"]

        # supportsSpecificESSStandard
        capability = eval_result_re3data_entry["metrics"]["supportsSpecificESSStandard"]
        ms_domains = list(repo_metadata.objects(metadata_standard, NFDICO.subjectArea))
        logger.debug(
            f"Repo {repo_name} supports domains {ms_domains} for metadata standard {ms_title}."
        )
        for domain in ms_domains:
            if str(domain) in scientific_domains_ess:
                capability["result"] = True
                capability["reason"]["supports"].append(str(ms_title[0]) if len(ms_title)==1 else str(ms_title))
                capability["singlemetricscore"]["score_achieved"] = capability["singlemetricscore"]["score_max_possible"]
                break

    logger.debug(
        f' * supportsCrossDomainStandard: {eval_result_re3data_entry["metrics"]["supportsCrossDomainStandard"]["reason"]["supports"]}'
    )
    logger.debug(
        f' * supportsSpecificESSStandard: {eval_result_re3data_entry["metrics"]["supportsSpecificESSStandard"]["reason"]["supports"]}'
    )
    return eval_result_re3data_entry


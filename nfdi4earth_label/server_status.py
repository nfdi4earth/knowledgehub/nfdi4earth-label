#!/usr/bin/env python
#
# Functions for checking API endpoints of repositories.

import logging
import requests.exceptions
from enum import Enum
from .re3data import WebApi
import ftplib

logger = logging.getLogger(__name__)


class ServerStatus(Enum):
    SERVER_DOWN = "DOWN"
    SERVER_UP_OK = "UP"
    SERVER_UP_ERROR = "ERROR"
    SERVER_UNKNOWN = "UNKNOWN"
    SERVER_ADDRESS_INVALID = "INVALID"


def probe_http_server_status(url: str) -> ServerStatus:
    """
    Probe whether an HTTP server is running at the given URL, e.g., ```https://www.google.com:80```.

    @param url: The URL to probe, e.g., ```https://www.google.com:80/```.
    @return: ServerStatus.SERVER_DOWN if the server is down, ServerStatus.SERVER_UP_OK if the server is up and responds with HTTP status 2xx or 3xx, and ServerStatus.SERVER_UP_ERROR if the server is up but responds with HTTP status 4xx or 5xx.
    """
    try:
        r = requests.get(url, timeout=5)
        r.raise_for_status()  # Raises a HTTPError if the status is 4xx, 5xxx
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
        return ServerStatus.SERVER_DOWN
    except requests.exceptions.HTTPError:
        return ServerStatus.SERVER_UP_ERROR
    else:
        return ServerStatus.SERVER_UP_OK


def is_ftp_url_maybe(url: str) -> bool:
    """Check whether this could be a valid FTP URL. Many re3data entries list HTTP or HTTPS URLs as FTP URLs. Sometimes these are web server listing of
    the FTP directory, sometimes the URLs are landing pages with information about the FTP server, or they are just wrong. We could use urlparse to check
    more details, but this is not necessary for our purposes."""
    return url.startswith("ftp://")


def is_http_or_https_url_maybe(url: str) -> bool:
    """Check whether this could be a valid HTTP or HTTPS URL. We could use urlparse to check more details, but this is not necessary for our purposes."""
    return url.startswith("http://") or url.startswith("https://")


def probe_ftp_server_status(url: str) -> ServerStatus:
    """
    Probe whether an FTP server is running at the given URL, e.g., ```ftp://
    @param url: The URL to probe, e.g., ```ftp://
    @return: ServerStatus.SERVER_DOWN if the server is down, ServerStatus.SERVER_UP_OK if the server is up and responds with HTTP status 2xx or 3xx, and ServerStatus.SERVER_UP_ERROR if the server is up but responds with HTTP status 4xx or 5xx.
    """

    ftp_client = ftplib.FTP()
    ftp_client.set_pasv(True)
    print(f"******* Trying  FTP connection to '{url}'. *******")
    try:
        ftp_client.connect(url)
        print(f"    FTP connection to {url} established: OK.")
        response = ftp_client.login()  # anonymous login, user anonymous, passwd anonymous@
        # now list directory contents
        # _ = ftp_client.nlst()
        # ftp_client.quit()
        return ServerStatus.SERVER_UP_OK
    except ftplib.all_errors as e:
        print(f"    FTP connection to {url} FAILED.")
        return ServerStatus.SERVER_DOWN


def check_api_endpoints(apis_to_endpoints: dict, repo_name: str = None) -> dict:
    """
    Check the status of the given API endpoints.

    @param apis_to_endpoints: A dictionary mapping API types to endpoints, e.g., {"OAI-PMH": "https://www.pangaea.de/oai/provider"}
    @param repo_name: Optional str, the name of the repository to check. If provided, it will be included in the log messages.
    @return: A dictionary mapping API types to status values, e.g., {"OAI-PMH": "UP"}.
    """
    api_status = {}
    api_idx = 0
    for api_type, endpoint in apis_to_endpoints.items():
        if api_type in [WebApi.API_OAI_PMH.value, WebApi.API_REST.value]:
            status = probe_http_server_status(endpoint)
            logger.info(
                f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' has status: {status.value}"
            )
            api_status[api_type] = status.value
        elif api_type == WebApi.API_FTP.value:
            if is_ftp_url_maybe(endpoint):
                status = probe_ftp_server_status(endpoint)
                logger.info(
                    f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' has status: {status.value}"
                )
                api_status[api_type] = status.value
            elif is_http_or_https_url_maybe(
                endpoint
            ):  # Many entries in re3data have type FTP, but are actually HTTP/HTTPS URLs.
                status = probe_http_server_status(endpoint)
                logger.info(
                    f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' has status: {status.value}"
                )
                if status == ServerStatus.SERVER_UP_OK:
                    logger.warning(
                        f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' is a HTTP/HTTPS URL but listed in re3data as FTP URL."
                    )
                api_status[api_type] = status.value
            else:
                logger.warning(
                    f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' is not a valid FTP or HTTP/HTTPS URL. Skipping."
                )
                api_status[api_type] = ServerStatus.SERVER_ADDRESS_INVALID.value
        else:
            api_status[api_type] = ServerStatus.SERVER_UNKNOWN.value
            logger.warning(
                f" -Repo '{repo_name}' API #{api_idx} of type '{api_type}' at endpoint '{endpoint}' is not supported by this tool. Skipping."
            )
    return api_status

#!/usr/bin/env python
#
# Functions for querying OGC CSW endpoints.
#
# Uses owslib, see https://owslib.readthedocs.io/en/latest/usage.html#csw

import logging
from owslib.csw import CatalogueServiceWeb

logger = logging.getLogger(__name__)


# It seems we can test this using this repo: https://api.bas.ac.uk/data/metadata/csw/v2/published?


def probe_csw_operations(url: str, repo_name=None):
    """
    Probe whether an OGC CSW server is running at the given URL, e.g., ```https://api.bas.ac.uk/data/metadata/csw/v2/```, and return supported operations.

    @param url: The URL to probe, e.g., ```https://api.bas.ac.uk/data/metadata/csw/v2/```.
    @param repo_name: The name of the repository, used for logging.
    @return: tuple of bool, list. The bool is True if the server is up and responds with HTTP status 2xx or 3xx, False if the server is down or responds with HTTP status 4xx or 5xx. The list of str contains the operations the server supports, and is empty if the server is down or responds with HTTP status 4xx or 5xx.
    """
    supported_operations = []

    if url is None:
        return False, supported_operations

    logger.debug(f"Trying CSW connection to '{url}'.")

    try:
        csw = CatalogueServiceWeb(url)
        csw.identification.type
        supported_operations = [op.name for op in csw.operations]
        logger.info(f"******** CSW server for repo '{repo_name}' is up and supports operations: {supported_operations}")
    except Exception as e:
        logger.debug(f"Error while probing for CSV server for repo '{repo_name}': {e}")
        return False, supported_operations
    else:
        return True, supported_operations

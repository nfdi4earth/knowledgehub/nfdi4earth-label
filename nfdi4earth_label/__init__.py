# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

"""nfdi4earth-label

nfdi4earth-label
"""

from __future__ import annotations
import logging
import sys

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Tim Schaefer, Jonas Grieb, Freya Thießen"
__copyright__ = "2023 NFDI4Earth"
__credits__ = [
    "Tim Schaefer",
    "Jonas Grieb",
    "Freya Thießen",
]
__license__ = "Apache-2.0"

__maintainer__ = "Tim Schaefer"
__email__ = "tim.schaefer@senckenberg.de"

__status__ = "Pre-Alpha"


# Package-wide logger configuration.
logger = logging.getLogger(__name__)
# logger.setLevel(logging.DEBUG)   # configure the default log level here. logging.INFO or logging.DEBUG should do it.
logger.setLevel(logging.INFO)
sh = logging.StreamHandler(sys.stdout)
fmt_interactive = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s", "%H:%M:%S")
sh.setFormatter(fmt_interactive)
logger.addHandler(sh)

from .settings import check_label_app_status, get_settings
# Check essential settings for the Label App and warn on critical issues at startup.
if not get_settings().get("SKIP_STARTUP_CHECKS", False):
    check_label_app_status(silent=True, raise_exception=False, apptag="[CheckLabelAppStatus @startup] ")


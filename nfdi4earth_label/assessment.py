
import logging
from dataclasses import dataclass
from enum import Enum

from typing import List


logger = logging.getLogger(__name__)

class SubAsmType(Enum):
    """Simple enum for sub assessment types."""
    RE3DATA = "re3data"
    FUJI = "fuji"
    SELFASSESSMENT = "selfassessment"

class SubAsmFilter(Enum):
    """Simple enum for filtering sub assessment types."""
    NONE = "none"
    OFFICIAL = "official"
    LATEST = "latest"


def get_subassessments(assessment: dict, asmtype : SubAsmType, subasmfilter : SubAsmFilter) -> List[dict]:
    """Get sub assessments from the assessment dict based on the type and filter. The assessment dict is from the Cordra API."""
    subassessments = assessment["content"]["results"][asmtype.value]
    asmtype_key = f"{asmtype.value}_subassessment_uuid"
    if subasmfilter == SubAsmFilter.OFFICIAL or subasmfilter == SubAsmFilter.LATEST:
        uuid_of_instance_for_asmtype_and_category = assessment["content"]["results"]["achievedN4ELabel"][subasmfilter.value]["based_on_subassessments"][asmtype_key]
        subassessments = [subasm for subasm in subassessments if subasm["uuid"] == uuid_of_instance_for_asmtype_and_category]
    return subassessments

def get_subassessment_with_uuid(assessment: dict, asmtype : SubAsmType, uuid : str) -> dict:
    """Get sub assessment from the assessment dict based on the type and uuid. The assessment dict is from the Cordra API."""
    subassessments = assessment["content"]["results"][asmtype.value]
    subassessments = [subasm for subasm in subassessments if subasm["uuid"] == uuid]
    if len(subassessments) == 1:
        return subassessments[0]
    return None

from enum import Enum


class WebApi(Enum):
    """Simple enum for web API types used by repositories."""

    API_REST = "REST"
    API_FTP = "FTP"
    API_NETCDF = "NetCDF"
    API_OAI_PMH = "OAI-PMH"
    API_OGC_CSW = "OGC CSW"

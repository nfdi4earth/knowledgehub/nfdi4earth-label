# This file defines the scoring functions for the sub assessments of the NFDI4Earth label.
# The sub assessments are re3data, fuji, and selfassessment.
# This is the single source of truth for the scoring of the assessment.

from typing import List, Dict, Union
from logging import getLogger

logger = getLogger(__name__)

# The hard-coded thresholds for the scores required to get the label.
# Note that, in addition to these thresholds, the individual metrics can be marked as required for the label or not,
# see the 'required_for_label' field in the 'metrics' dictionary below.
def get_re3data_score_required_to_get_label():
    return 4

def get_fuji_score_required_to_get_label():
    return 0

def get_selfassessment_score_required_to_get_label():
    return 4


metrics = {
    "re3data" : {
        "assignsPersistentIds": {
            "suggestion": "Please assign persistent identifiers.",
            "required_for_label": False,  # The 'required_for_label' field determines whether a positive result for this metric (in the metrics.<metricname>.result field) is required to get the label. Or, to put it another way, if this is set to True, any repo that does not fullfill this metric will not get the label.
            "max_score_possible": 1
        },
        "assignsUniqueIds": {
            "suggestion": "Please assign unique identifiers.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "providesAnyAPI": {
            "suggestion": "Please provide an API.",
            "required_for_label": True,
            "max_score_possible": 1
        },
        "supportsApisHarvestable": {
            "suggestion": "Please support harvestable APIs.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "supportsCrossDomainStandard": {
            "suggestion": "Please support cross-domain standards.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "supportsSpecificESSStandard": {
            "suggestion": "Please support specific ESS standards.",
            "required_for_label": False,
            "max_score_possible": 1
        }
    },
    "fuji" : {
        "hasDescriptiveMetadata": {
            "suggestion": "Please provide descriptive metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "metadataIncludesIdentifier": {
            "suggestion": "Please include a data identifier in the metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "metadataLinks": {
            "suggestion": "Please provide links in the metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "includesLicense": {
            "suggestion": "Please include a license in metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "includesProvenance": {
            "suggestion": "Please include provenance information in metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "metadataRepresentation": {
            "suggestion": "Please provide metadata representation.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "specifiesContent": {
            "suggestion": "Please specify the content of data in metadata.",
            "required_for_label": False,
            "max_score_possible": 1
        }
    },
    "selfassessment" : {
        "hasBackupStrategy": {
            "suggestion": "Please implement a backup strategy. Backups are essential to ensure that data is not lost in case of hardware failure or other issues. Most institutions have backup strategies in place or offer central backup services, but it is important to use them and document the process, including what is backup up in what intervals, and what needs to be done in order to restore a backup.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "hasArchivingStrategy": {
            "suggestion": "Please implement an archiving strategy. Data archiving is the process of moving data to a separate storage device for long-term retention. This is important to ensure that data is not lost and can be accessed in the future.",
            "required_for_label": False,
            "max_score_possible": 1
        },
        "hasCuration": {
            "suggestion": "Please implement a curation strategy. Data curators are responsible for the selection, preservation, maintenance, collection and archiving of digital assets. If you have some form of data curation, this should be a step of the process of uploading data. It is fine if this are automated methods that check for the validity or availability of certain metadata fields.",
            "required_for_label": False,
            "max_score_possible": 1,
            "extra_reason_fields" : {   # These are fields under the 'result' field that are specific to this metric. Every metric has the fields "description" of type string. All other fields are listed here. See the schema of RepositoryEvalForLabel in the knowledgehub-backend-setup repo for the fields. This dictionary is used to copy the relevant fields to the selfassessment part of the evaluation result when a user fills our the self-assessment form in the frontend.
                "curation_metadata" : "boolean",
                "curation_fileformats" : "boolean",
                "curation_data" : "boolean",
            }
        },
        "hasUserSupport": {
            "suggestion": "Please provide some form of user support information. This can be a helpdesk, a forum, a mailing list, a Discord channel, or similar. If you do not have the resources to help users with dedicated staff, it is sufficient to provide information on where users can talk to each other and ask the community for help.",
            "required_for_label": False,
            "max_score_possible": 1,
            "extra_reason_fields" : {   # These are fields under the 'result' field that are specific to this metric. Every metric has the fields "description" of type string. All other fields are listed here. See the schema of RepositoryEvalForLabel in the knowledgehub-backend-setup repo for the fields. This dictionary is used to copy the relevant fields to the selfassessment part of the evaluation result when a user fills our the self-assessment form in the frontend.
                "helpdesk_has" : "boolean",
                "helpdesk_url" : "string",
                "userinteraction_has" : "boolean",
                "userinteraction_url" : "string",
            }

        },
        "hasFundingStatement": {
            "suggestion": "Please provide a funding statement. A funding statement should include information on the funding sources of the repository, the funding period, and the funding amount. If the repository is not funded, please state this explicitly.",
            "required_for_label": False,
            "max_score_possible": 1,
            "extra_reason_fields" : {   # These are fields under the 'result' field that are specific to this metric. Every metric has the fields "description" of type string. All other fields are listed here. See the schema of RepositoryEvalForLabel in the knowledgehub-backend-setup repo for the fields. This dictionary is used to copy the relevant fields to the selfassessment part of the evaluation result when a user fills our the self-assessment form in the frontend.
                "url" : "string",
            }
        }
    }
}

# The selfassessment for aks for extra certifications, but the answer does not influence the automated score computation of the label.
# Label team members will have to discuss on a case-by-case basis whether the certifications are sufficient to get the label.
non_metrics = {
    "re3data" : {},
    "fuji" : {},
    "selfassessment" : {
        "hasCertifications": {
            "suggestion": "",
            "required_for_label": False,
            "max_score_possible": 0,
            "extra_reason_fields" : {   # These are fields under the 'result' field that are specific to this metric. Every metric has the fields "description" of type string. All other fields are listed here. See the schema of RepositoryEvalForLabel in the knowledgehub-backend-setup repo for the fields. This dictionary is used to copy the relevant fields to the selfassessment part of the evaluation result when a user fills our the self-assessment form in the frontend.
                "name" : "string",
                "url" : "string",
            }
        }
    }
}

def get_max_score_possible_for_re3data():
    return sum([metrics["re3data"][metric]["max_score_possible"] for metric in metrics["re3data"]])

def get_max_score_possible_for_fuji():
    return sum([metrics["fuji"][metric]["max_score_possible"] for metric in metrics["fuji"]])

def get_max_score_possible_for_selfassessment():
    return sum([metrics["selfassessment"][metric]["max_score_possible"] for metric in metrics["selfassessment"]])



def compute_subassessmentscore_for_re3data(acontent: Dict) -> Dict:
    """
    Compute the score for the re3data part of the evaluation result. Computes all required singlemetricscore entries first.
    @param acontent: the JSON datastructure of the re3data part of the evaluation result.
    @return the modified acontent, with the score fields (the 'subassessmentscore' and all 'singlemetricscore' fields) filled in.
    """
    return compute_subassessmentscore_for_subassessment(acontent, "re3data")


def compute_subassessmentscore_for_fuji(acontent: Dict) -> Dict:
    """
    Compute the score for the fuji part of the evaluation result. Computes all required singlemetricscore entries first.
    @param acontent: the JSON datastructure of the fuji part of the evaluation result.
    @return the modified acontent, with the score fields (the 'subassessmentscore' and all 'singlemetricscore' fields) filled in.
    """
    return compute_subassessmentscore_for_subassessment(acontent, "fuji")




def _compute_singlemetricscore_for_subassessment(metric_name : str, acontentpartformetric: Dict, asmtype : str) -> Dict:
    """
    Compute the score for a single metric of the selfassessment part of the evaluation result.
    @param metric_name: the name of the metric to compute the score for. You can find the names in the 'metrics' dictionary above, or in the schema.
    @param acontentpartformetric: part of the JSON datastructure of the selfassessment part of the evaluation result, for a single metric.
    @param asmtype: the type of the subassessment, e.g. "re3data", "fuji", or "selfassessment".
    @return the modified acontentpartformetric, with the score fields filled in.
    """
    logger.debug(f"Computing singlemetricscore for {asmtype} metric {metric_name} : {str(acontentpartformetric)}")
    score_max_possible = metrics[asmtype][metric_name]["max_score_possible"]
    acontentpartformetric["singlemetricscore"]["score_max_possible"] = score_max_possible

    if acontentpartformetric["result"] == True:
        score_achieved = score_max_possible  # If the result is positive, the score achieved is the maximum possible score for now.
    else:
        score_achieved = 0
    acontentpartformetric["singlemetricscore"]["score_achieved"] = score_achieved

    acontentpartformetric["singlemetricscore"]["score_not_achieved_suggestion"] = metrics[asmtype][metric_name]["suggestion"]
    acontentpartformetric["singlemetricscore"]["required_for_label"] = metrics[asmtype][metric_name]["required_for_label"]
    return acontentpartformetric



def compute_subassessmentscore_for_selfassessment(acontent: Dict) -> Dict:
    """
    Compute the score for the selfassessment part of the evaluation result. Computes all required singlemetricscore entries first.
    @param acontent: the JSON datastructure of the selfassessment part of the evaluation result.
    @return the modified acontent, with the score fields (the 'subassessmentscore' and all 'singlemetricscore' fields) filled in.
    """
    return compute_subassessmentscore_for_subassessment(acontent, "selfassessment")


def compute_subassessmentscore_for_subassessment(acontent: Dict, asmtype : str) -> Dict:
    """
    Compute the score for the selfassessment part of the evaluation result. Computes all required singlemetricscore entries first.
    @param acontent: the JSON datastructure of the selfassessment part of the evaluation result.
    @param asmtype: the type of the subassessment, e.g. "re3data", "fuji", or "selfassessment".
    @return the modified acontent, with the score fields (the 'subassessmentscore' and all 'singlemetricscore' fields) filled in.
    """
    score_metrics_considered = list(metrics[asmtype].keys())

    for metric_name in score_metrics_considered:
        acontent["metrics"][metric_name] = _compute_singlemetricscore_for_subassessment(metric_name, acontent["metrics"][metric_name], asmtype)

    return _compute_subassessmentscore_for_any_subassessment(acontent, score_metrics_considered, asmtype=asmtype)


## common functions


def _compute_subassessmentscore_for_any_subassessment(acontent: Dict, score_metrics_considered : List[str], asmtype : str) -> Dict:
    """
    Compute the score for any sub assessment.
    @param acontent: the JSON datastructure of the sub assessment part of the evaluation result.
    @param score_metrics_considered: the list of metrics to consider for the score.
    @param asmtype: the type of the subassessment, e.g. "re3data", "fuji", or "selfassessment".
    @return the modified acontent, with the score fields filled in.
    """
    score_achieved = 0
    score_max_possible = 0
    log_tag = f" {asmtype}"

    for metric in score_metrics_considered:
        score_max_possible += acontent["metrics"][metric]["singlemetricscore"]["score_max_possible"]
        score_achieved += acontent["metrics"][metric]["singlemetricscore"]["score_achieved"]

    logger.debug(f"_compute_subassessmentscore_for_any_subassessment: {log_tag} computed score {score_achieved} / {score_max_possible} for {len(score_metrics_considered)} metrics: {score_metrics_considered}")

    acontent["subassessmentscore"]["score_achieved"] = score_achieved
    acontent["subassessmentscore"]["score_max_possible"] = score_max_possible
    acontent["subassessmentscore"]["score_metrics_considered"] = score_metrics_considered
    if asmtype == "re3data":
        acontent["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_re3data(acontent)
    elif asmtype == "fuji":
        acontent["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_fuji(acontent)
    elif asmtype == "selfassessment":
        acontent["subassessmentscore"]["subassessment_passed"] = compute_subasm_passed_selfassessment(acontent)
    else:
        raise ValueError(f"Unknown asmtype {asmtype}")

    return acontent


def compute_subasm_passed_re3data(re3data_assessment : Union[Dict, None]) -> bool:
    if re3data_assessment is None:
        return False
    re3data_score = re3data_assessment["subassessmentscore"]["score_achieved"]
    re3data_api_check_passed = re3data_assessment["metrics"]["providesAnyAPI"]["result"] == True
    re3data_score_passed = re3data_score >= get_re3data_score_required_to_get_label()
    re3data_passed = re3data_score_passed and re3data_api_check_passed
    return re3data_passed

def compute_subasm_passed_fuji(fuji_assessment : Union[Dict, None]) -> bool:
    return True # Currently we do not require a F-UJI assessment, as many repos have no suitable API. So this is always True.

def compute_subasm_passed_selfassessment(selfassessment_assessment : Union[Dict, None]) -> bool:
    if selfassessment_assessment is None:
        return False
    selfassessment_score = selfassessment_assessment["subassessmentscore"]["score_achieved"]
    selfassessment_score_passed = selfassessment_score >= get_selfassessment_score_required_to_get_label()
    selfassessment_passed = selfassessment_score_passed
    return selfassessment_passed
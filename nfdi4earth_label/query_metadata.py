#!/usr/bin/env python

from typing import List
import rdflib
from SPARQLWrapper import SPARQLWrapper, JSONLD, JSON
import logging


logger = logging.getLogger(__name__)

from .settings import get_settings

settings = get_settings()
CORDRA_API_URL = settings.get("CORDRA_API_URL")
SPARQL_ENDPOINT = settings.get("SPARQL_ENDPOINT")

n4e_namespace = "http://nfdi4earth.de/ontology/"
NFDICO = rdflib.Namespace("https://nfdi.fiz-karlsruhe.de/ontology/")
N4E = rdflib.Namespace(n4e_namespace)

SUBJECT_AREA_SPARQL_ENDPOINT = settings.get("SUBJECT_AREA_SPARQL_ENDPOINT")

cached_ess_subject_areas: List[str] = []

ess_subject_area_query = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>

SELECT * {
  ?s a owl:Class .
  # use a SPARQL Property Path expression to get all classes where any of
  # their parents classes (iterates full hierarchy) is dfgfo:34
  ?s rdfs:subClassOf* <https://github.com/tibonto/dfgfo/34> .
}
"""

query_4earth_repos: str = """PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX schema: <http://schema.org/>
PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX org: <http://www.w3.org/ns/org#>

SELECT DISTINCT ?repo_id ?repo_name
WHERE{
  ?repo_id a n4e:Repository .
  ?nfdi4earthProject schema:url "https://nfdi4earth.de/"^^xsd:anyURI .
  ?organizationIRI org:hasMembership ?membership .
  ?membership org:organization ?nfdi4earthProject .
  {
    ?repo_id dct:publisher ?organizationIRI .
  }UNION {
    # In the KH, sometimes also the suborganization of a direct NFDI4Earth member organization
    # is responsible for a service, therefore we need this additional UNION statement
    ?subOrg org:subOrganizationOf ?organizationIRI .
    ?subOrg foaf:name ?NFDI4EarthPublisher .
    FILTER NOT EXISTS {
      ?repo_id dct:publisher ?anyPublisher .
      ?anyPublisher org:hasMembership ?membership2 .
      ?membership2 org:organization ?nfdi4earthProject
    }
  }
  ?repo_id dct:title ?repo_name .
}"""

query_all_repos: str = """PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX org: <http://www.w3.org/ns/org#>

SELECT DISTINCT ?repo_id ?repo_name
WHERE{
  ?repo_id a n4e:Repository .
  {
    ?repo_id dct:publisher ?organizationIRI .
  }
  ?repo_id dct:title ?repo_name .
}"""

query_get_all_api_types: str = """PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT ?repositoryName ?standardName ?apiType
WHERE {
  ?standard a n4e:MetadataStandard .
  ?standard dct:title ?standardName .
  ?repo a n4e:Repository .
  ?repo dct:title ?repositoryName .
  ?repo n4e:supportsMetadataStandard ?standard .
  ?repo n4e:hasAPI ?api .
  ?api dct:conformsTo ?apiType
}"""


# Query to retrieve all eval result (assessment datastructure) for a single repo.
# Note that the database may contain several assessments for a single repo.
# Of course, the database may also contain no assessment at all for a given repo.
query_get_eval_results_for_repo: str = """PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT DISTINCT ?evalresult
WHERE {
  ?evalresult a n4e:RepositoryEvalForLabel .
  ?evalresult n4e:repository ?repo .
  ?repo dct:title ?reponame .
  FILTER(STR(?reponame) = "REPO_NAME")
}"""

# Query to retrieve all evalutation results for all repos.
query_get_all_eval_results: str = """PREFIX n4e: <http://nfdi4earth.de/ontology/>
PREFIX dct: <http://purl.org/dc/terms/>

SELECT DISTINCT ?evalresult
WHERE {
  ?evalresult a n4e:RepositoryEvalForLabel .
}"""


def get_ess_subject_areas() -> List[str]:
    if len(cached_ess_subject_areas) > 0:
        return cached_ess_subject_areas
    else:
        sparql = SPARQLWrapper(SUBJECT_AREA_SPARQL_ENDPOINT)
        sparql.setQuery(ess_subject_area_query)
        sparql.setReturnFormat(JSON)
        result_dict = sparql.query().convert()
        ess_subject_areas = [str(binding["s"]["value"]) for binding in result_dict["results"]["bindings"]]
        cached_ess_subject_areas.extend(ess_subject_areas)
        return cached_ess_subject_areas



def query_all_api_types_sparql() -> rdflib.graph.Graph:
    """
    Execute SPARQL query on knowledge hub and return API type list. This is for all repos, not just the 4earth ones.
    :return: rdflib.graph.Graph object with API type information
    """
    sparql = SPARQLWrapper(SPARQL_ENDPOINT)
    sparql.setQuery(query_get_all_api_types)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


def get_repositories_sparql(
    only4earth: bool = True,
    strip_url_from_handle: bool = True,
) -> rdflib.graph.Graph:
    """
    Execute SPARQL query on knowledge hub and return repository list.
    @param only4earth: whether to return only repos which are part of the NFDI4Earth
    @return rdflib.graph.Graph object with repository information
    """
    sparql = SPARQLWrapper(SPARQL_ENDPOINT)
    query = query_4earth_repos if only4earth else query_all_repos
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    repos_dict = sparql.query().convert()

    if strip_url_from_handle:
        # Remove the URL part from the handle
        for repo_idx, repo in enumerate(repos_dict["results"]["bindings"]):
            object_handle = repo["repo_id"][
                "value"
            ]  # something like 'http://localhost:8080/objects/n4e/r3d-r3d100010299'
            object_id = object_handle.split("/objects/", 1)[
                1
            ]  # only the part 'n4e/r3d-r3d100010299'
            repos_dict["results"]["bindings"][repo_idx]["repo_id"][
                "value"
            ] = object_id
            logger.debug(
                f"Replaced object handle '{object_handle}' with '{object_id}'."
            )
    return repos_dict


query_4earth_repo_details: str = """prefix dcat: <http://www.w3.org/ns/dcat#>
prefix dct: <http://purl.org/dc/terms/>
prefix n4e: <http://nfdi4earth.de/ontology/>
prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix nfdico: <https://nfdi.fiz-karlsruhe.de/ontology/>
CONSTRUCT {
  <REPO_ID> rdf:type n4e:Repository .
  #<REPO_ID> dcat:theme ?domain .
  <REPO_ID> n4e:assignsIdentifierScheme ?identifierScheme .
  <REPO_ID> n4e:catalogAccessType ?access .
  <REPO_ID> dct:language ?lang .
  <REPO_ID> n4e:hasAPI ?api .
  ?api dct:conformsTo ?apiStandard .
  ?api dcat:endpointURL ?endpointURL .
  <REPO_ID> n4e:supportsMetadataStandard ?metaStandardObj .
  ?metaStandardObj dct:title ?metaStandard .
  ?metaStandardObj nfdico:subjectArea ?metaStDomain .
  <REPO_ID> n4e:catalogLicense ?repoLicense .
  <REPO_ID> n4e:dataLicense ?dataLicense .
}
WHERE {
  <REPO_ID> rdf:type n4e:Repository .
  #<REPO_ID> dcat:theme ?domain .
  OPTIONAL{<REPO_ID> n4e:assignsIdentifierScheme ?identifierScheme .}
  OPTIONAL{<REPO_ID> n4e:catalogAccessType ?access .}
  OPTIONAL{<REPO_ID> dct:language ?lang .}
  OPTIONAL{
    <REPO_ID> n4e:hasAPI ?api .
    ?api dct:conformsTo ?apiStandard .
    ?api dcat:endpointURL ?endpointURL .}
  OPTIONAL{
    <REPO_ID> n4e:supportsMetadataStandard ?metaStandardObj .
    ?metaStandardObj dct:title ?metaStandard .
    OPTIONAL {
      ?metaStandardObj nfdico:subjectArea ?metaStDomain .
    }
  }
  OPTIONAL{<REPO_ID> n4e:catalogLicense ?repoLicense .}
  OPTIONAL{<REPO_ID> n4e:dataLicense ?dataLicense .}
}"""


def get_metadata(repo_id: str) -> rdflib.graph.Graph:
    """
    Execute SPARQL query on knowledge hub and return metadata for a single repository.

    :param repo_id: the repository identifier (URI) for which metadata should be retrieved.
    :return: serialized RDF graph, converted to a Python object. See https://sparqlwrapper.readthedocs.io/en/latest/main.html#return-formats. Note that the exact result type depends on the query type (CONSTRUCT vs SELECT vs ...). For a CONSTRUCT query, the result is of type https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.ConjunctiveGraph.
    """
    repo_iri = CORDRA_API_URL + "objects/" + repo_id
    id_query = query_4earth_repo_details.replace("REPO_ID", repo_iri)
    sparql = SPARQLWrapper(SPARQL_ENDPOINT)
    sparql.setQuery(id_query)
    sparql.setReturnFormat(JSONLD)
    return sparql.query().convert()

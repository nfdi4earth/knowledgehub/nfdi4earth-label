
import random
from typing import Dict, Union, List

def get_singlemetric_score_object(score_max_possible : int, method_or_fixed_achieved_value : Union[str, int], score_not_achieved_suggestion : str = "", required_for_label : bool = False) -> Dict:

    if isinstance(method_or_fixed_achieved_value, str) and not method_or_fixed_achieved_value in ["max", "min", "random"]:
        raise ValueError("method_or_fixed_achieved_value must be one of 'max', 'min', 'random' if it is a string")

    if method_or_fixed_achieved_value == "max":
        score_achieved = score_max_possible
    elif method_or_fixed_achieved_value == "min":
        score_achieved = 0
    elif method_or_fixed_achieved_value == "random":
        score_achieved = random.randint(0, score_max_possible)
    else:
        assert isinstance(method_or_fixed_achieved_value, int)
        score_achieved = method_or_fixed_achieved_value
    return {
        "score_achieved": score_achieved,
        "score_max_possible": score_max_possible,
        "score_not_achieved_suggestion": score_not_achieved_suggestion,
        "required_for_label": required_for_label
    }


def get_subassessment_score_object(score_achieved : int = 0, score_max_possible : int = 0, score_metrics_considered : List[str] = [], subassessment_passed : Union[bool, str] = "auto") -> Dict:
    if subassessment_passed == "auto":
        subassessment_passed = score_achieved == score_max_possible
    assert isinstance(subassessment_passed, bool)
    return {
        "score_achieved": score_achieved,
        "score_max_possible": score_max_possible,
        "score_metrics_considered": score_metrics_considered,
        "subassessment_passed" : subassessment_passed
    }


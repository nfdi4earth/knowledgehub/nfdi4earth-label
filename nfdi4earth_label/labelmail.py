#!/usr/bin/env python

"""
Functions to send emails via SMTP.
"""

import logging
import smtplib, ssl
from email.mime.text import MIMEText

from .settings import get_settings

settings = get_settings()
SMTP_SERVER = settings.get("SMTP_SERVER")
SMPT_PORT = settings.get("SMPT_PORT")
LABEL_TEAM_EMAIL = settings.get("LABEL_TEAM_EMAIL")
LABEL_SMTP_SECRET_MAIL_USER = settings.get("LABEL_SMTP_SECRET_MAIL_USER")
LABEL_SMTP_SECRET_MAIL_PASSWORD = settings.get("LABEL_SMTP_SECRET_MAIL_PASSWORD")


logger = logging.getLogger(__name__)


def send_label_contact_email(subject, messagebody, sendername, replyemail):
    if not '@' in replyemail:
        logger.error(f"send_label_contact_email: Cannot send email: Invalid reply email address '{replyemail}'. Expected @ symbol in email address.")
        return
    logger.info(f"send_label_contact_email: Trying to send email from '{sendername}' (email: {replyemail}) with subject '{subject}'.")
    message_subject = f"N4ELabel Contact Form: {subject}"
    message_body = f"Dear NFDI4Earth Label Team member,\n\nthe following message was received via the NFDI4Earth Label contact form.\n\nSender name: {sendername}\nSender email for replies: {replyemail}\nMessage Body: {messagebody}\n\n-- \nHave a great day."
    mime_message = get_mime_email(message_subject, message_body, recipient_email=LABEL_TEAM_EMAIL)
    _send_message(mime_message)


def send_label_application_as_repository_representative_email(appl_email, appl_name, appl_organization, repoid):
    logger.info(f"send_label_application_as_repository_representative_email: Trying to send repo rep application email from '{appl_name}' (email: {appl_email}) for repo '{repoid}'.")
    message_subject = f"N4ELabel Repository Representative Form request"
    message_body = f"\n\nDear NFDI4Earth Label Team member,\n\nthe following message was received via the NFDI4Earth Label Repository Represenative Form. The following request was made:\n\nApplicant name: {appl_name}\nApplicant email: {appl_email}\nApplicant Organization: {appl_organization}\nApplication as representative for repository: {repoid}.\n\n--\nHave a great day."
    mime_message = get_mime_email(message_subject, message_body, recipient_email=LABEL_TEAM_EMAIL)
    _send_message(mime_message)


def send_testemail(recipient_email):
    logger.info(f"send_testemail: Trying to send test email to '{recipient_email}'.")
    message_subject = f"N4ELabel Test Email"
    message_body = "Dear NFDI4Earth Label Team member,\n\nthis is a test email to check if the SMTP server is working.\n\n--\nHave a great day."
    mime_message = get_mime_email(message_subject, message_body, recipient_email)
    _send_message(mime_message)


def get_mime_email(subject : str, message_body : str, recipient_email : str = LABEL_TEAM_EMAIL):
    mime_message = MIMEText(message_body, "plain")
    mime_message["Subject"] = subject
    mime_message["From"] = LABEL_SMTP_SECRET_MAIL_USER
    mime_message["To"] = recipient_email
    return mime_message


def _send_message(mime_message):
    if LABEL_SMTP_SECRET_MAIL_USER is None or LABEL_SMTP_SECRET_MAIL_PASSWORD is None:
        logger.error("Cannot send email: No SMTP credentials provided.")
        return
    context = ssl.create_default_context()
    to_address = mime_message["To"]
    with smtplib.SMTP(SMTP_SERVER, SMPT_PORT) as server:
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(LABEL_SMTP_SECRET_MAIL_USER, LABEL_SMTP_SECRET_MAIL_PASSWORD)
        server.sendmail(from_addr=LABEL_SMTP_SECRET_MAIL_USER, to_addrs=to_address, msg=mime_message.as_string())
        server.quit() # close SMTP connection
#!/usr/bin/env python

import pandas as pd

from .re3data import upload_repo_eval_result, compute_repo_re3data_eval_result, WebApi
from .cordra_util import get_cordra_cfg, ensure_repo_assessment_in_cordra
from .fuji_data import fuji_eval, FujiEvalStatus, add_fuji_eval_result_to_eval_result, FujiEvalResult
from .server_status import check_api_endpoints, ServerStatus
from .query_metadata import get_repositories_sparql, query_all_api_types_sparql
from .csw import probe_csw_operations
from typing import Union, NoReturn
from .fuji_data import get_fuji_cfg
import logging
import json
from collections import Counter
from .re3data import get_eval_default
import argparse
import sys
from .common import AssessmentPart, get_repo_api_url, get_api_endpoints


logger = logging.getLogger(__name__)  # You can set the default log level in the file `__init__.py`.


def get_api_type_counts_from_database() -> tuple[dict, int]:
    """
    Get all API types that are in the database, along with the number of occurrences. This information is based on re3data.
    Currently the SPARQL query is not limited to 4earth repositories.

    @return 2-tuple, first entry: dict mapping API type (str) to number of occurrences (int). second entry: int, total number of repositories in the database.
    """
    repo_api_info = query_all_api_types_sparql()
    api_types = dict()
    num_repos_total: int = len(get_repositories_sparql(only4earth=False)["results"]["bindings"])
    for repo_info in repo_api_info["results"]["bindings"]:
        try:
            api_type = repo_info["apiType"]["value"]
            if not api_type in api_types:
                api_types[api_type] = 1
            else:
                api_types[api_type] += 1
        except KeyError:
            pass
    return api_types, num_repos_total


def get_repo_id_for_name(query_repo_name: str) -> Union[str, None]:
    """
    Get the repository ID for a given repository name. This function is not used in the main evaluation code, but it is useful for debugging and testing.

    @param repo_name: The name of the repository.
    @return: The ID of the repository in the Knowledge Hub database, or None if no such repo exists in the database.
    """
    ids, names = get_all_repos()
    for repo_idx, repo_name in enumerate(names):
        if repo_name == query_repo_name:
            return ids[repo_idx]
    return None


def get_all_repos() -> tuple:
    """
    Get all repositories from the knowledge hub Fuseki database. This function is not used in the main evaluation code, but it is useful for debugging and testing.

    @return: A tuple of lists, the first one holds the repo IDs in the database, the second holds the repo names.
    """
    repos_dict = get_repositories_sparql(only4earth=True)
    df = pd.DataFrame.from_dict(pd.json_normalize(repos_dict["results"]["bindings"]))
    if df.empty:
        return [], []
    return df["repo_id.value"].tolist(), df["repo_name.value"].tolist()


def eval_single_repo(
    repo_name: str,
    repo_id: str,
    cordra_cfg: dict = None,
    fuji_cfg: dict = None,
    upload_result: bool = False,
    test_only: bool = False,
) -> dict:
    """
    Run the full evaluation for a single repository. This is the main function called by the 'evalrepo' command line application.

    Parameters
    ----------
    repo_name : str
        The repository name in the Knowledge Hub cordra database. Comes from re3data.
    cordra_cfg : dict, optional
        CORDRA configuration, by default None and will be obtained from `../knowledge-hub-backend-setup/variables.env`.
    fuji_cfg : dict, optional
        F-UJI configuration dict with fields `user` and `password`, by default None, meaning that default values ('fuji', 'fuji') will be used.
    upload_result: bool, whether to upload result to cordra
    """
    if cordra_cfg is None:
        cordra_cfg = get_cordra_cfg("../knowledge-hub-backend-setup/variables.env")
    if fuji_cfg is None:
        fuji_cfg = get_fuji_cfg()

    logger.debug(f"CORDRA config: {cordra_cfg}")

    if repo_id is None or repo_name is None:
        logger.error(f"Invalid repository data: repo_name and repo_id must not be None.")
        return None

    # First, do our own evaluation.
    eval_result = compute_repo_re3data_eval_result(repo_id, repo_name)

    repo_oaipmh_api_url = get_repo_api_url(eval_result, WebApi.API_OAI_PMH)

    if repo_oaipmh_api_url is None:
        logger.info(f"Skipping F-UJI evaluation for repo '{repo_id}' because no OAI-PMH API URL was found.")
        # fuji_eval_result = FujiEvalResult(FujiEvalStatus.NO_EVAL_DONE_NO_API, None, None, None)
        fuji_eval_result = None
    else:
        logger.info(
            f"Running F-UJI evaluation for repo '{repo_id}' with API URL '{repo_oaipmh_api_url}'. This will fail if the F_UJI service is not running. F-UJI config: {str(fuji_cfg)}"
        )
        fuji_eval_result = fuji_eval(repo_oaipmh_api_url, fuji_cfg, repo_name=repo_name)
        logger.info(
            f" -F-UJI evaluation status for repo {repo_name}: {fuji_eval_result.status_code} ({fuji_eval_result.status_string})"
        )
        if fuji_eval_result.was_successful:
            logger.info(f"   *F-UJI evaluation result for repo {repo_name}: {fuji_eval_result.fuji_overview}")

    relevant_parts = [AssessmentPart.RE3DATA.value]

    if fuji_eval_result is not None:
        eval_result = add_fuji_eval_result_to_eval_result(eval_result, fuji_eval_result)
        relevant_parts.append(AssessmentPart.FUJI.value)

    # Now upload if requested.
    if upload_result:
        try:
            upload_repo_eval_result(
                eval_result,
                relevant_parts,
                cordra_cfg=cordra_cfg,
                create_repo_if_not_exists=test_only,
            )
        except Exception as e:
            logger.error(f"Could not upload evaluation result for {repo_id} ({repo_name}) to CORDRA: {str(e)}")

    return eval_result


# ---------------------------------- MAIN of evalrepos command line programm -------------------------------------
def run_for_all_repos(
    cordra_cfg: dict = None, fuji_cfg: dict = None, test_only: bool = False
) -> NoReturn:
    """
    Run the full evaluation for all repositories in the knowledge hub. This is the main function called by the 'evalrepos' command line application.
    You may want to run for inidividual repos with the lbl_runassessment command line tool instead.
    Running this function has the advantage that it prints some statistics in the end.

    Parameters
    ----------
    cordra_cfg : dict, optional
        CORDRA configuration, by default None and will be obtained from `../knowledge-hub-backend-setup/variables.env`.
    fuji_cfg : dict, optional
        F-UJI configuration dict with fields `user` and `password`, by default None, meaning that default values ('fuji', 'fuji') will be used.
    test_only : bool, optional. Alternatively, an integer can be provided, which will be interpreted as the number of repositories to evaluate.
        If True, only one repo is evaluated (PANGAEA).
    """

    parser = argparse.ArgumentParser(
        description="Run Label Assessment (re3data and F-UJI if possible) for all 4Earth Repos in the KH and save to DB"
    )

    parser.add_argument("--allrepos", action="store_true", help="Include all repos, even non-4Earth ones.")
    parser.add_argument("--testrun", action="store_true", help="Perform a test run with a single repo (PANGAEA).")
    parser.add_argument(
        "--prepare",
        action="store_true",
        help="Do not run assessments, but create the empty default data structures holding the assessments for all repos in cordra.",
    )
    parser.add_argument(
        "--run",
        action="store_true",
        help="Whether to really run something. This is a safeguard against accidentally running assessments, it needs to be set for the app to do anything.",
    )
    args = parser.parse_args()

    only4earth: bool = not args.allrepos
    test_only: bool = args.testrun
    prepare: bool = args.prepare

    if not args.run:
        print(f"Parameter '--run' not specified, not doing anything. Please run with '-h' to see usage help.")
        sys.exit(0)

    if only4earth:
        logger.info("Using only repositories which are part of the NFDI4Earth.")
    else:
        logger.info("Using all repositories, including ones which are not part of the NFDI4Earth.")

    if cordra_cfg is None:
        cordra_cfg = get_cordra_cfg()
    if fuji_cfg is None:
        fuji_cfg = get_fuji_cfg()

    logger.debug(f"CORDRA config: {cordra_cfg}")
    repos_dict = get_repositories_sparql(
        only4earth=only4earth, strip_url_from_handle=True
    )  # We want only 4earth repositories in production.

    repos = pd.DataFrame.from_dict(pd.json_normalize(repos_dict["results"]["bindings"]))
    logger.info(f"Found {len(repos)} repositories to evaluate.")
    if repos.empty:
        logger.warning(f"Found no repositories to evaluate in database, nothing to do. (Check database?)")
        return

    if prepare:
        logger.info(f"Creating empty assessment datastructures for {len(repos)} repositories.")
        num_new = 0
        for repo_id, repo_name in zip(repos["repo_id.value"], repos["repo_name.value"]):
            # names_from_db = get_repo_names_for_id(repo_id, cordra_cfg=cordra_cfg)
            # if repo_name not in names_from_db:
            #    logger.warning(f"Retrieved repo names '{names_from_db}' for repo with id {repo_id} from cordra, but local name is '{repo_name}'.")
            did_exist, _ = ensure_repo_assessment_in_cordra(repo_id, repo_name=repo_name, cordra_cfg=cordra_cfg)
            num_new += int(not did_exist)
        logger.info(
            f"Created {num_new} new empty assessment datastructures for {len(repos)} repositories. Exiting. (No actual assessments computed.)"
        )
        sys.exit(0)

    # This collects general statistics about the API types in the database, not limited to 4Earth repos. It is FYI only, and not used further. We are interested in this to identify the API types that are most common.
    api_types, num_considered_repos = get_api_type_counts_from_database()
    num_apis_total = sum([num_apis for num_apis in api_types.values()])
    logger.info(
        f"Found {len(api_types)} different API types in the full database for {num_considered_repos} repos: {api_types} ({num_apis_total} APIs total)."
    )

    # TODO:
    # - could we include aggregators in the evaluation?

    repos_with_api_id = (
        list()
    )  # Together with repos_with_api_name, this is a list of all repositories that have an API we tested. The ids map to the names.
    repos_with_api_name = list()  # see above

    repos_with_api_fuji_status = (
        list()
    )  # For repos which have an OAI PMH API we tested, this is a list of the F-UJI evaluation status codes.
    num_repos_total = 0  # Number of evaluated repositories in total (4earth repos)
    num_repos_with_api_up_total = 0  # Number of evaluated repositories that have any API up (4earth repos)

    re3data_listed_api_count_by_type = (
        dict()
    )  # dict mapping API type (str) to number of occurrences in re3data records (int)
    active_api_count_by_type = (
        dict()
    )  # dict mapping API type (str) to number of actually available APIS (int). note that some endpoints which are listed are not really API endpoints but just websites listing information on how to access the API, and that we do not test all API types, the untested ones are not counted as online.
    for api in [a.value for a in WebApi]:
        active_api_count_by_type[api] = 0
        re3data_listed_api_count_by_type[api] = 0

    apis_by_repo = dict()  # dict of dicts, saves for each repo a dict of API types to endpoints.
    is_test_mode = False

    # Do the full evaluation for each repo.
    for repo_id, repo_name in zip(repos["repo_id.value"], repos["repo_name.value"]):
        if isinstance(test_only, bool):
            if test_only == True:
                is_test_mode = True
                if repo_name != "PANGAEA":
                    continue
        elif isinstance(test_only, int) and test_only > 0:
            is_test_mode = True
            if num_repos_total >= test_only:
                break
        else:
            sys.exit("test_only must be of type int or bool")

        num_repos_total += 1
        relevant_parts = []

        # First, do our own evaluation.
        eval_result = compute_repo_re3data_eval_result(repo_id, repo_name)
        relevant_parts.append(AssessmentPart.RE3DATA.value)

        # Check for non-OAI-PMH APIs
        apitypes_to_endpoints = get_api_endpoints(eval_result)
        apis_by_repo[repo_name] = apitypes_to_endpoints

        for api_type in apitypes_to_endpoints.keys():
            re3data_listed_api_count_by_type[api_type] += 1
            if api_type == WebApi.API_REST.value:
                logger.warning(
                    f"########## REST API found for repo '{repo_name}' at URL '{apitypes_to_endpoints[api_type]}'."
                )

        api_status = check_api_endpoints(apitypes_to_endpoints, repo_name=repo_name)
        num_apis_up = sum([1 for status in api_status.values() if status == ServerStatus.SERVER_UP_OK.value])
        if len(apitypes_to_endpoints) > 0:
            if num_apis_up > 0:
                num_repos_with_api_up_total += 1
            apis_up = [
                api_type for api_type, api_status in api_status.items() if api_status == ServerStatus.SERVER_UP_OK.value
            ]
            logger.info(f" -Repo '{repo_name}' has {num_apis_up} of {len(api_status)} APIs up: {apis_up}.")

            for api_type in apis_up:
                active_api_count_by_type[api_type] += 1
        else:
            logger.debug(f" -Repo '{repo_name}' has no APIs listed in re3data.")

        # Check for CSW API
        repo_csw_api_url = get_repo_api_url(eval_result, WebApi.API_OGC_CSW)
        probe_csw_operations(repo_csw_api_url, repo_name=repo_name)
        # We also try APIs listed as REST APIs, because some of them are actually CSW APIs.
        repo_rest_api_url = get_repo_api_url(eval_result, WebApi.API_REST)
        probe_csw_operations(repo_rest_api_url, repo_name=repo_name)

        do_skip_fuji = False  # speed up during debugging other stuff, F-UJI is slow
        if do_skip_fuji:
            logger.warning(f"Skipping F-UJI evaluation for repo '{repo_id}'.")
            continue

        # Now run F-UJI. This requires the F-UJI service to be running.
        repo_oaipmh_api_url = get_repo_api_url(eval_result, WebApi.API_OAI_PMH)

        add_fuji_eval_part = False
        if repo_oaipmh_api_url is None:
            logger.info(f"Skipping F-UJI evaluation for repo '{repo_id}' because no OAI-PMH API URL was found.")
            fuji_eval_result = FujiEvalResult(FujiEvalStatus.NO_EVAL_DONE_NO_API, None, None, None, FujiEvalStatus.getDetailJsonTemplate())
        else:
            logger.info(
                f"Running F-UJI evaluation for repo '{repo_id}' with API URL '{repo_oaipmh_api_url}'. This will fail if the F_UJI service is not running. F-UJI config: {str(fuji_cfg)}"
            )
            add_fuji_eval_part = True
            repos_with_api_id.append(repo_id)
            repos_with_api_name.append(repo_name)
            fuji_eval_result = fuji_eval(repo_oaipmh_api_url, fuji_cfg, repo_name=repo_name)
            repos_with_api_fuji_status.append(fuji_eval_result.status_code)
            logger.info(
                f" -F-UJI evaluation status for repo {repo_name}: {fuji_eval_result.status_code} ({fuji_eval_result.status_string})"
            )
            if fuji_eval_result.was_successful:
                logger.info(f"   *F-UJI evaluation result for repo {repo_name}: {fuji_eval_result.fuji_overview}")

        if add_fuji_eval_part:
            relevant_parts.append(AssessmentPart.FUJI.value)
            eval_result = add_fuji_eval_result_to_eval_result(eval_result, fuji_eval_result)

        if test_only == True:
            filename = f"evalresult_{repo_name}.json"
            with open(filename, "w", encoding="utf-8") as file:
                json.dump(eval_result, file, ensure_ascii=False, indent=4)
            logger.info(f"Test mode active: Wrote evaluation result for '{repo_name}' to file '{filename}'.")

            filename_empty_eval = "evalresult_empty.json"
            with open(filename_empty_eval, "w", encoding="utf-8") as efile:
                json.dump(get_eval_default(), efile, ensure_ascii=False, indent=4)
            logger.info(f"Test mode active: Wrote empty default evaluation result to file '{filename_empty_eval}'.")

        # Now upload
        try:
            upload_repo_eval_result(
                eval_result,
                relevant_parts,
                cordra_cfg=cordra_cfg,
                create_repo_if_not_exists=is_test_mode,
            )
        except Exception as e:
            logger.error(f"(allrepos) Could not upload evaluation result for {repo_id} ({repo_name}) to CORDRA: {e}")

    if isinstance(test_only, bool) and test_only == True:
        logger.warning("Test mode active: Only evaluated a single repo: PANGAEA.")
    elif isinstance(test_only, int) and test_only > 0:
        logger.warning(f"Test mode active: Only evaluated {test_only} repos.")

    logger.info(f"")
    logger.info(f"Handled {num_repos_total} repositories in total, {num_repos_with_api_up_total} have any API up.")
    logger.info(
        f"In total, {len(repos_with_api_id)} of them have an OAIPMH API we tested: {repos_with_api_name} (skip F-UJI is: {do_skip_fuji})."
    )
    for statuscode, num_occurrences in Counter(repos_with_api_fuji_status).items():
        logger.info(
            f" -F-UJI evaluation status code {statuscode} occured {num_occurrences} times (meaning: {FujiEvalStatus.descriptions[statuscode]})"
        )

    logger.info(f"")
    logger.info(f"--Detailed information for each repo with an OAIPMH API:--")
    for idx, repo_name in enumerate(repos_with_api_name):
        logger.info(f" -Result for repo '{repo_name}': {FujiEvalStatus.descriptions[repos_with_api_fuji_status[idx]]}")

    logger.info(f"")
    logger.info(f"--Number of repositories with each API type up:--")
    for api_type, num_repos in active_api_count_by_type.items():
        logger.info(f" -{api_type}: {num_repos}")

    logger.info(f"")
    logger.info(f"--APIs by repository:--")
    num_no_api = 0
    for repo_name, apis in apis_by_repo.items():
        if not apis:
            num_no_api += 1
            continue
        logger.info(f" -{repo_name}: {apis}")
    logger.info(f" -{num_no_api} of {len(apis_by_repo)} repositories have no APIs listed in re3data.")

    logger.info(f"")
    logger.info(f"--API types listed in re3data (for {len(repos)} repos, APIs not tested for availability):--")
    for api_type, num_repos in re3data_listed_api_count_by_type.items():
        logger.info(f" -for API type {api_type}: {num_repos} repos with this API type listed in re3data.")

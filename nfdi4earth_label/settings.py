# Settings used throughout the application

import os
from typing import Dict, Any
from dotenv import load_dotenv
load_dotenv()

# env var LABEL_APP_MODE must be set to one of 'production', 'staging', 'development'
LABEL_APP_MODE = os.getenv('LABEL_APP_MODE', 'development').lower()

def get_settings() :
    if LABEL_APP_MODE == 'production':
        return settings_production
    elif LABEL_APP_MODE == 'staging':
        return settings_staging
    else:
        if LABEL_APP_MODE != 'development':
            print(f"Warning: Unrecognized LABEL_APP_MODE: {LABEL_APP_MODE}. Defaulting to development settings.")
        return settings_development

settings_production = {

    "IS_PRODUCTION" : True,
    "SKIP_STARTUP_CHECKS" : False,
    "DISABLE_AUTH_FOR_TESTING" : False,  # Note: in production environment this should never be True!
    "BACKEND_BASE_URL" : "http://localhost:8000", # Used to construct Label link
    "VARIABLES_ENV_FILE" : "../knowledge-hub-backend-setup/variables.env",
    "FUJI_API_URL" : "http://localhost:1071/fuji/api/v1/evaluate",
    "FUJI_USER" : "fuji",
    "FUJI_PASSWORD" : "fuji",
    "CORDRA_API_URL" : os.getenv('CORDRA_BASE_URI', "http://localhost:8080/"),
    "FRONTEND_VERIFY_URL" : "http://localhost:3000/verify",  # Used to generate the Label badge, which links to this
    "SPARQL_ENDPOINT" : "http://localhost:3030/knowledge-graph/query",
    "SUBJECT_AREA_SPARQL_ENDPOINT" : "https://sparql.knowledgehub.nfdi4earth.de/dfgfo/query",
    "CORS_ORIGINS" : [
        "http://localhost",
        "http://localhost:3000",  # React frontend
        "http://127.0.0.1:3000",
        "http://localhost:8000",  # FastAPI backend
        "http://0.0.0.0:3000",
        "http://0.0.0.0:5173", # OneStop Frontend
        "http://localhost:5173",
        # SSL
        "https://localhost",
        "https://localhost:3000",  # React frontend
        "https://127.0.0.1:3000",
        "https://localhost:8000",  # FastAPI backend
        "https://0.0.0.0:3000",
        "https://0.0.0.0:5173", # OneStop Frontend
        "https://localhost:5173"
    ],
    "AUTHZ_ASSESS_REPO_ATTRIBUTE" : "canAssessRepos",
    "AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE" : "isLabelTeamMember",

    # Used for sending email directly via SMTP when form requests are received.
    "ENABLE_SMTP" : False,  # Whether the backend supports sending email via the endpoint for the contact form. Set to False to disable email sending. The other SMTP settings are ignored in that case.
    "SMTP_SERVER" : "msx.tu-dresden.de",
    "SMPT_PORT" : 587,
    "LABEL_SMTP_SECRET_MAIL_USER" : os.getenv('LABEL_SMTP_SECRET_MAIL_USER'),  # The username (email address) for the SMTP server to send emails
    "LABEL_SMTP_SECRET_MAIL_PASSWORD" : os.getenv('LABEL_SMTP_SECRET_MAIL_PASSWORD'),     # The password for the SMTP server to send emails
    "SMTP_USE_TLS" : True,
    "SMTP_USE_SSL" : False,
    "LABEL_TEAM_EMAIL" : "you@your_isp",  # Email address of the label team, where to send contact form requests

    # Keycloak configuration
    "KEYCLOAK_CONFIG_JSON_FILEPATH" : "./keycloakConfig.json", # Required to get the keycloak configuration, including public key
}


settings_staging = {

    "IS_PRODUCTION" : False,
    "SKIP_STARTUP_CHECKS" : False,
    "DISABLE_AUTH_FOR_TESTING" : False,  # Note: in production environment this should never be True!
    "BACKEND_BASE_URL" : "https://labelbackend.knowledgehub.test.n4e.geo.tu-dresden.de", # Used to construct Label link
    "VARIABLES_ENV_FILE" : "../knowledge-hub-backend-setup/variables.env",
    "FUJI_API_URL" : "http://localhost:1071/fuji/api/v1/evaluate",
    "FUJI_USER" : "fuji",
    "FUJI_PASSWORD" : "fuji",
    "CORDRA_API_URL" : "http://localhost:8080/",
    "FRONTEND_VERIFY_URL" : "https://onestop4all.test.n4e.geo.tu-dresden.de/label/verify",  # Used to generate the Label badge, which links to this
    "SPARQL_ENDPOINT" : "https://sparql.knowledgehub.test.n4e.geo.tu-dresden.de",
    "SUBJECT_AREA_SPARQL_ENDPOINT" : "https://sparql.knowledgehub.test.n4e.geo.tu-dresden.de",
    "CORS_ORIGINS" : [
        "https://onestop4all.test.n4e.geo.tu-dresden.de",
    ],
    "AUTHZ_ASSESS_REPO_ATTRIBUTE" : "canAssessRepos",
    "AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE" : "isLabelTeamMember",

    # Used for sending email directly via SMTP when form requests are received.
    "ENABLE_SMTP" : False, # Whether the backend supports sending email via the endpoint for the contact form. Set to False to disable email sending. The other SMTP settings are ignored in that case.
    "SMTP_SERVER" : "msx.tu-dresden.de",
    "SMPT_PORT" : 587,
    "LABEL_SMTP_SECRET_MAIL_USER" : os.getenv('LABEL_SMTP_SECRET_MAIL_USER'),  # The username (email address) for the SMTP server to send emails
    "LABEL_SMTP_SECRET_MAIL_PASSWORD" : os.getenv('LABEL_SMTP_SECRET_MAIL_PASSWORD'),     # The password for the SMTP server to send emails
    "SMTP_USE_TLS" : True,
    "SMTP_USE_SSL" : False,
    "LABEL_TEAM_EMAIL" : "you@your_isp",  # Email address of the label team, where to send contact form requests

    # Keycloak configuration
    "KEYCLOAK_CONFIG_JSON_FILEPATH" : "../knowledge-hub-backend-setup/devops/keycloak/keycloakConfig.json", # Required to get the keycloak configuration, including public key
}


settings_development = {

    "IS_PRODUCTION" : False,
    "SKIP_STARTUP_CHECKS" : False,  # Whether to skip running the check_label_app_status() function in init
    "DISABLE_AUTH_FOR_TESTING" : False,  # Note: in production environment this should never be True!
    "BACKEND_BASE_URL" : "http://localhost:8000", # Used to construct Label link
    "VARIABLES_ENV_FILE" : "../knowledge-hub-backend-setup/variables.env",
    "FUJI_API_URL" : "http://localhost:1071/fuji/api/v1/evaluate",
    "FUJI_USER" : "fuji",
    "FUJI_PASSWORD" : "fuji",
    "CORDRA_API_URL" : "http://localhost:8080/",
    "FRONTEND_VERIFY_URL" : "http://localhost:3000/verify",  # Used to generate the Label badge, which links to this
    "SPARQL_ENDPOINT" : "http://localhost:3030",
    "SUBJECT_AREA_SPARQL_ENDPOINT" : "https://sparql.knowledgehub.nfdi4earth.de",
    "CORS_ORIGINS" : [
        "http://localhost",
        "http://localhost:3000",  # React frontend
        "http://127.0.0.1:3000",
        "http://localhost:8000",  # FastAPI backend
        "http://0.0.0.0:3000",
        "http://0.0.0.0:5173", # OneStop Frontend
        "http://localhost:5173",
        # SSL
        "https://localhost",
        "https://localhost:3000",  # React frontend
        "https://127.0.0.1:3000",
        "https://localhost:8000",  # FastAPI backend
        "https://0.0.0.0:3000",
        "https://0.0.0.0:5173", # OneStop Frontend
        "https://localhost:5173"
    ],
    "AUTHZ_ASSESS_REPO_ATTRIBUTE" : "canAssessRepos",
    "AUTHZ_LABELTEAMMMEMBER_ATTRIBUTE" : "isLabelTeamMember",

    # Used for sending email directly via SMTP when form requests are received.
    "ENABLE_SMTP" : False, # Whether the backend supports sending email via the endpoint for the contact form. Set to False to disable email sending. The other SMTP settings are ignored in that case.
    "SMTP_SERVER" : "msx.tu-dresden.de",
    "SMPT_PORT" : 587,
    "LABEL_SMTP_SECRET_MAIL_USER" : os.getenv('LABEL_SMTP_SECRET_MAIL_USER'),  # The username (email address) for the SMTP server to send emails
    "LABEL_SMTP_SECRET_MAIL_PASSWORD" : os.getenv('LABEL_SMTP_SECRET_MAIL_PASSWORD'),     # The password for the SMTP server to send emails
    "SMTP_USE_TLS" : True,
    "SMTP_USE_SSL" : False,
    "LABEL_TEAM_EMAIL" : "you@your_isp",  # Email address of the label team, where to send contact form requests

    # Keycloak configuration
    "KEYCLOAK_CONFIG_JSON_FILEPATH" : "./keycloakConfig.json", # Required to get the keycloak configuration, including public key
}


def check_label_app_status(silent : bool = True, raise_exception : bool = True, apptag : str = ""):
    """
    Check the status of the Label App. This is the main function for the lbl_appstatus command line script.

    @param silent: If True, suppresses output to the command line except for critical errors.
    @param raise_exception: If True, raises a ValueError for critical errors. If False, prints a message to the command line for critical errors
    """
    settings = get_settings()
    is_production = settings.get("IS_PRODUCTION")

    if not silent:
        print(f"{apptag}Checking Label App Status...")
        print(f"{apptag} * LABEL_APP_MODE: {LABEL_APP_MODE}")
        print(f"{apptag} * IS_PRODUCTION: {is_production}")
        print(f"{apptag} * DISABLE_AUTH_FOR_TESTING: {settings.get('DISABLE_AUTH_FOR_TESTING')}")

    if is_production and settings.get("DISABLE_AUTH_FOR_TESTING"):

        msg = f"{apptag}check_label_app_status: CRITICAL: IS_PRODUCTION is True, but DISABLE_AUTH_FOR_TESTING is also True. This is a security risk in production environment."
        if raise_exception:
            raise ValueError(msg)
        else:
            print(msg)

    if not os.path.isfile(settings.get("KEYCLOAK_CONFIG_JSON_FILEPATH")):
        msg = f"{apptag}CRITICAL: Keycloak configuration file not found at " + settings.get("KEYCLOAK_CONFIG_JSON_FILEPATH") + ". Please check the file path."
        if raise_exception:
            raise ValueError(msg)
        else:
            print(msg)

    if not os.path.isfile(settings.get("VARIABLES_ENV_FILE")):
        msg = f"{apptag}CRITICAL: variables.env file not found at " + settings.get("VARIABLES_ENV_FILE") + ". Please check the file path."
        if raise_exception:
            raise ValueError(msg)
        else:
            print(msg)

    if not settings_production.keys() == settings_staging.keys() == settings_development.keys():
        msg = f"{apptag}CRITICAL: Settings keys for production, staging, and development are not the same. Please check the settings and ensure all kes are set for all 3 setting dictionaries."
        if raise_exception:
            raise ValueError(msg)
        else:
            print(msg)

    if settings.get("ENABLE_SMTP"):
        if settings.get("LABEL_SMTP_SECRET_MAIL_USER") is None or settings.get("LABEL_SMTP_SECRET_MAIL_PASSWORD") is None:
            msg = f"{apptag}CRITICAL: No SMTP credentials provided, will not be able to send emails via contact form. Please set the LABEL_SMTP_SECRET_MAIL_USER and LABEL_SMTP_SECRET_MAIL_PASSWORD environment variables."
            if raise_exception:
                raise ValueError(msg)
            else:
                print(msg)
        else:
            # Check if the LABEL_SMTP_SECRET_MAIL_USER contains an @ symbol
            if "@" not in settings.get("LABEL_SMTP_SECRET_MAIL_USER"):
                msg = f"{apptag}CRITICAL: The LABEL_SMTP_SECRET_MAIL_USER environment variable does not contain an '@' symbol. Please check the value."
                if raise_exception:
                    raise ValueError(msg)
                else:
                    print(msg)

            else:
                if not silent:
                    print(f"{apptag}* Okay: SMTP credentials provided in environment variables.")



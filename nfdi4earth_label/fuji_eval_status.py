

class FujiEvalStatus:
    """
    Status codes for the F-UJI evaluation.
    """

    SUCCESS = 0
    FAILED_IDENTIFY = 1
    FAILED_LIST_IDENTIFIERS = 2
    FAILED_LIST_RECORDS = 3
    TOO_FEW_RECORDS = 4
    FAILED_GET_RECORD = 5
    FAILED_FUJI_FOR_RECORD = 6
    NO_EVAL_DONE_NO_API = 7
    FAILED_CUSTOM_PROCESSING = 8
    FAILED_FUJI_CONNECT = 9
    FAILED_FUJI_CONNECT_FAST = 9

    descriptions = {
        SUCCESS: "Success", # Everything worked out
        FAILED_IDENTIFY: "FailedIdentify",
        FAILED_LIST_IDENTIFIERS: "FailedListIdentifiers",
        FAILED_LIST_RECORDS: "FailedListRecords",
        TOO_FEW_RECORDS: "TooFewRecords",
        FAILED_GET_RECORD: "FailedGetRecord",
        FAILED_FUJI_FOR_RECORD: "FailedFujiForRecord",
        NO_EVAL_DONE_NO_API: "NoEvalDoneNoApi",
        FAILED_CUSTOM_PROCESSING: "FailedCustomProcessing",
        FAILED_FUJI_CONNECT: "FailedToConnectToFujiService",
        FAILED_FUJI_CONNECT_FAST: "FailedToConnectToFujiServiceAtStart"
    }

    FoundApiUrl = "FoundApiUrl"
    IdentifySuccessful = "IdentifySuccessful"
    ListIdentifiersSuccessful = "ListIdentifiersSuccessful"
    ListRecordsSuccessful = "ListRecordsSuccessful"
    ListRecordsNotEmpty = "ListRecordsNotEmpty"
    AccessOneRecordSuccessful = "AccessOneRecordSuccessful"
    AccessAllRequestedRecordsSuccessful = "AccessAllRequestedRecordsSuccessful"
    FujiScanSuccessOneRecord = "FujiScanSuccessOneRecord"
    FujiScanSuccessAllRecords = "FujiScanSuccessAllRecords"
    FujiConnectSuccessful = "FujiConnectSuccessful"

    @staticmethod
    def getDetailJsonTemplate():
        return {
            FujiEvalStatus.FoundApiUrl : {
                "result": False,
                "description" : "Found URL for a standardized API to use for F-UJI assessment in the re3data record",
            },
            FujiEvalStatus.IdentifySuccessful : {
                "result": False,
                "description" : "Could connect to repository API and request identification",
                "details" : "",
            },
            FujiEvalStatus.ListIdentifiersSuccessful : {
                "result": False,
                "description" : "Could list dataset identifiers",
                "details" : "",
            },
            FujiEvalStatus.ListRecordsSuccessful : {
                "result": False,
                "description" : "Could list dataset identifiers",
                "details" : "",
            },
            FujiEvalStatus.ListRecordsNotEmpty : {
                "result": False,
                "description" : "Repository dataset listing was non-empty",
                "details" : "",
            },
            FujiEvalStatus.AccessOneRecordSuccessful: {
                "result": False,
                "description" : "Could access details for at least one record",
                "details" : "",
            },
            FujiEvalStatus.AccessAllRequestedRecordsSuccessful: {
                "result": False,
                "description" : "Could access details for all requested records",
                "details" : "",
            },
            FujiEvalStatus.FujiScanSuccessOneRecord : {
                "result": False,
                "description" : "Could run F-UJI assessment for at least one record",
                "details" : "",
            },
            FujiEvalStatus.FujiScanSuccessAllRecords : {
                "result": False,
                "description" : "Could run F-UJI assessment for all requested records",
                "details" : "",
            },
            FujiEvalStatus.FujiConnectSuccessful : {
                "result": False,
                "description" : "Whether our system could connect to the F-UJI service for assessing the repository.",
                "details" :  "An error in this field indicates an internal server error in the NFDI4Earth infrastructure and should be reported to the NFDI4Earth Label team."
            }
        }

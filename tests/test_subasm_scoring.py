# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

from nfdi4earth_label.subasm_scoring import metrics, non_metrics, get_max_score_possible_for_re3data, get_max_score_possible_for_fuji, get_max_score_possible_for_selfassessment

def test_metrics():
    assert len(list(metrics.keys())) == 3, "Expected 3 keys (re3data, fuji, selfassessment) in metrics"
    assert "re3data" in metrics.keys(), "Expected 're3data' in metrics"
    assert "fuji" in metrics.keys(), "Expected 'fuji' in metrics"
    assert "selfassessment" in metrics.keys(), "Expected 'selfassessment' in metrics"


def test_non_metrics():
    assert len(list(non_metrics.keys())) == 3, "Expected 3 keys (re3data, fuji, selfassessment) in metrics"
    assert "re3data" in non_metrics.keys(), "Expected 're3data' in non_metrics"
    assert "fuji" in non_metrics.keys(), "Expected 'fuji' in non_metrics"
    assert "selfassessment" in non_metrics.keys(), "Expected 'selfassessment' in non_metrics"


def test_get_max_score_possible_for_re3data():
    # currently all weights are 1, so this should hold
    assert get_max_score_possible_for_re3data() == len(metrics.get("re3data").keys())


def test_get_max_score_possible_for_fuji():
    # currently all weights are 1, so this should hold
    assert get_max_score_possible_for_fuji() == len(metrics.get("fuji").keys())


def test_get_max_score_possible_for_selfassessment():
    # currently all weights are 1, so this should hold
    assert get_max_score_possible_for_selfassessment() == len(metrics.get("selfassessment").keys())
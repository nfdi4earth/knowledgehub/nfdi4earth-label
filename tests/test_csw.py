# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from nfdi4earth_label.csw import probe_csw_operations

@pytest.mark.remoteservices
def test_probe_csw_operations():
    csw_api_url = "https://api.bas.ac.uk/data/metadata/csw/v2/"
    is_up, capabilities = probe_csw_operations(csw_api_url)
    assert is_up == True
    assert 'GetRecords' in capabilities
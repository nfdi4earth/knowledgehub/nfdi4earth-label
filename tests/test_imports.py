# Copyright 2023 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import nfdi4earth_label  # noqa: F401

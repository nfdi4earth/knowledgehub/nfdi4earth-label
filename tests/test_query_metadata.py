# Copyright 2023, 2024 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

from nfdi4earth_label.query_metadata import get_ess_subject_areas

def test_get_ess_subject_areas():
    subject_areas = get_ess_subject_areas()
    assert len(subject_areas) > 0, f"Expected at least one subject area, got {len(subject_areas)}"
    assert "https://github.com/tibonto/dfgfo/313-01" in subject_areas, f"Expected 'blah' to be in subject areas, got {subject_areas}"



# Copyright 2023 NFDI4Earth
#
# SPDX-License-Identifier: Apache-2.0

from nfdi4earth_label.fuji_data import fuji_eval, FujiEvalStatus
import pytest

@pytest.mark.slow
@pytest.mark.remoteservices
@pytest.mark.fuji
def test_fuji_data():
    # This requires the F-UJI docker container to be running.
    # It also takes quite a bit of time.
    gfz = "https://doidb.wdc-terra.org//oaip/oai"
    fuji_cfg = { "user": "fuji", "password": "fuji"  }  # Get the required auth data from the file <fuji-repo>/fuji_server/config/users.py
    fuji_eval_res = fuji_eval(gfz, fuji_cfg)
    assert fuji_eval_res.status_code in [FujiEvalStatus.SUCCESS, FujiEvalStatus.FAILED_IDENTIFY, FujiEvalStatus.FAILED_LIST_IDENTIFIERS, FujiEvalStatus.FAILED_LIST_RECORDS, FujiEvalStatus.TOO_FEW_RECORDS, FujiEvalStatus.FAILED_GET_RECORD, FujiEvalStatus.FAILED_FUJI_FOR_RECORD, FujiEvalStatus.NO_EVAL_DONE_NO_API, FujiEvalStatus.FAILED_CUSTOM_PROCESSING, FujiEvalStatus.FAILED_FUJI_CONNECT]
# Repository metadata evaluation

To support the development of the NFDI4Earth label, we analyze the completeness of repositories metadata in the NFDI4Earth Knowledge Hub (KH). The majority of the metadata there was harvested from [re3data](https://www.re3data.org/).

This repository contains a Jupyter Notebook and a set of queries which are used within the notebook to query the SPARQL endpoint of the KH.

## Use directly in Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2Fnfdi4earth%2Fmetadatastandardsandcommons%2Fnfdi4earth-label/5c5d16b6d4b2f97dd2148d103d8189be8c04a9ab?urlpath=lab%2Ftree%2Frepository-metadata-eval%2Frepository-evaluation.ipynb)

## Installation

This notebook uses Python3.8.

To use this locally, the recommended steps are:

```
# optional, but recommended to use a virtual environment
sudo apt-get install python3.8-venv
# Python interpreter version 3.8 might also be simply linked as python on your system
python3.8 -m venv .
# now activate the virtual environment
source bin/activate
# install the required Python libraries
# (requirements.txt must be in the root of the repository for it to be binder-ready, contains libs for the notebook)
pip install -r ../requirements.txt
# for a local installation also jupyter is needed:
pip install jupyter
```

## Run

Run in the command line `jupyter notebook` and then open in the interactive Jupyter window in the browser `repository-evaluation.ipynb`

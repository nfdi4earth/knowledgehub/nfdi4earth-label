---
name: Obtaining the NFDI4Earth Label for Your Repository
# The title of the article.

description:
# A brief summary of the article, max. 300 characters incl. spaces.
This article describes the process for obtaining the NFDI4Earth Label for a research data repository in the earth system sciences.
The article is written for repository representatives who are interested in obtaining the Label and want to understand the application
process so they can judge the required effort and are well prepared for the assessment.

author:
  - name: Tim Schäfer
    orcidId: https://orcid.org/0000-0002-3683-8070
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed.

inLanguage: ENG
# The language of the article. Accepted values are:
# ENG for English
# DEU for German

about:
  - nfdi4earth Label
  - research data repository
  - research data management
  - certification


isPartOf:
  - LHB_ENG.md
  - Collection_StoreManage.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs.
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names.

additionalType: workflow
# The type of the article. Acceptable terms are:
# article: The most generic category. Should only be used if none of the others fit.
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology.
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic.
# user_guide: Manuals on how to use e.g., software products.
# isPartOf: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs.
# topical_entry: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the topical entry points.
# call_participate: Can only used by the NFDI4Earth Living Handbook Editorial Board. Identifies the articles featured in the "How to participate" section on of the OS4A Portal start page.

subjectArea:
  - http://vocabularies.unesco.org/thesaurus/concept7396  # "Data exchange"
# Neither of the DFG nor the UNESCO have something more specific like "research data management" or "certification", it seems.
#
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from:
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - research data repository
  - FAIR data
  - certification
  - evaluation
  - research data management
  - NFDI4Earth Label
# A list of terms to describe the article's topic more precisely.

audience:
  - data provider
  - service provider
  - data advocate
  - data collector
# A list of the main roles of persons within research data management that is addressed by the article.
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling.
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines.
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term.
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle them to create insights and knowledge.
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system.
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization.
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so.
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers.
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups.

identifier:
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345).

version: 0.1
# The version of the article; will be updated by the editor.

license: CC-BY-4.0
# The license of the article. Must not be altered.
# Note by article author (Tim Schaefer): I would also be willing to publish under CC0.
---

# Obtaining the NFDI4Earth Label for Your Repository

This article descibes the assessment process that is required for obtaining the NFDI4Earth Label for a repository. It is targeted at repository representatives who are interested in obtaining the Label.


## About the NFDI4Earth Label

The landscape of research data repositories in the earth system sciences (ESS) is very heterogeneous, with different standards used across sub disciplines and repositories. Furthermore, information on the standards used by a certain repository is often lacking, which makes it hard to automatically combine data from different repositories or identify a suitable repository for a given research project. The NFDI4Earth Label is a community based project to assess and improve the current level of interoperability and trustworthiness of ESS data repositories by providing a set of repository guidelines and clear metrics to assess the degree to which a repository conforms to these guidelines.

Repositories can run a self assessment using the provided guidelines and metrics, and those that reach a certain score threshold during an official assessment have the right to show the NFDI4Earth Label badge on their website, and will have the same badge displayed in relevant parts of the NFDI4Earth website.

The assessment for obtaining the NFDI4Earth Label consists of three parts:

* Part I: re3data evaluation
* Part II: F-UJI evaluation (optional, requires suitable API)
* Part III: Evaluation based on the self assessment form



## Overview of the assessment procedure

The workflow is visualized in Figure 1 below. It starts with the applicant providing the [re3data](https://re3data.org) identifier of the repository and applying for the Label. The identifier is used to automatically retrieve required information on the repository from re3data. Since re3data does not yet have fields for all required information, and some of the required information contains personal data that is not suitable for public display at re3data, an additional self-assessment form will need to be filled out on the NFDI4Earth Label website. Some technical capabilities of the repository are then queried automatically using [F-UJI software](https://www.f-uji.net/) if possible. The F-UJI software is a FAIR assessment tool for datasets. We sample several datasets stored in a repository and run a F-UJI evaluation on them, and then aggregate the results to arrive at a single score that represents the metadata quality in the repository. The information from re3data, the self-assessment form, and F-UJI is then used in combination with the metrics to perform the assessment.

The assessment result consists of a numeric score, recommendations on how to improve the FAIRness of the repository and thus the score, and, if the score exceeds a certain threshold, the achieved Label badge. The badge is planned as an image that holds the repository name and a text stating that the repository has achieved the NFDI4Earth Label. The image is clickable and links to a URL on the NFDI4Earth website where it can be verified by users. Internally, the badge will be implemented by using a service like shields.io.
This preliminary assessment result is communicated to the repository representatives who applied for the Label for review, to ensure that the results of the evaluation are representative of the capabilities of the repository, and that the automated tests did not miss any capabilities of the repository, e.g., due to software problems, a service down time or other technical issues. If consultancy is needed, members of the Label team will help to investigate any issues. Once the result is considered final, it will be communicated again, and will subsequently be made public. This includes updating the NFDI4Earth database with the information that the repository has obtained the Label, so that it is automatically displayed at relevant pages and can be verified by users. The repository should also update its website to include the Label badge in a prominent location.


![The process of obtaining the NFDI4Earth Label](https://git.rwth-aachen.de/nfdi4earth/knowledgehub/nfdi4earth-label/-/raw/dev/resources/workflow/Label_Application_Workflow_Abstract.drawio.png "The process of obtaining the NFDI4Earth Label")
*Figure 1: The process of obtaining the NFDI4Earth Label.*


## Checklist for the Assessment

### Before you start the assessment

* Make sure all relevant stake holders at your institution have read the [Label guidelines for repositories](https://label.nfdi4earth.de/guidelines) and agree that they intend to work towards implementing them for the repository that you intend to get certified with the Label.
* Make sure your repository is registered at [re3data](https://re3data.org). If not, register it. Make sure you know the re3data identifier of the repository.
* Designate a technical contact person and an organizational contact person that will communicate with the NFDI4Earth Label team during the full assessment procedure. Have their contact information (email addresses) ready.
* Using the official [Guidelines and Metrics document of the NFDI4Earth Label](https://label.nfdi4earth.de/guidelines), perform a self evaluation to get a rough impression of where you currently stand. Ignore the F-UJI part of the assessment for now. Think about how you could improve and consider taking action now for requirements which you do not fulfill yet, but consider low-hanging fruit.

### During the assessment

* Start the official assessment by filling out the [Label Application Form](https://label.nfdi4earth.de/apply).
* You will be contacted via email on the address you specified in the form. You will be provided with contact information of the NFDI4Earth Label team and with credentials for your account on the NFDI4Earth Label website.
* Members of the Label team will run a software to perform the re3data evaluation (Part I of the assessment).
* If your repository has a suitable application programming interface (API) URL listed in re3data, members of the NFDI4Earth will run a F-UJI assessment against the repository (Part II of the assessment).
* You will be asked to fill out the Self Assessment Form (Part III of the assessment) on the NFDI4Earth website. Note: You can preview the guidelines and metrics used to evaluate the form in the [Label guidelines for repositories](https://label.nfdi4earth.de/guidelines). Fill out the form if all the questions are clear. If you have questions, please contact the Label team.
* Members of the Label team may contact you with questions regarding the assessment results.
* Members of the Label team will contact you and communicate to you (and only to you) the preliminary assessment results. It is now your turn to compare the results with your expectations and discuss them with the Label team. Once you and the Label team agree that the assessment results do indeed represent the capabilities of the repository, this phase ends.
* The assessment becomes official. The score is made public, and you receive the badge URL.

### After the assessment

* The badge can be displayed on the repository website.


Note: The certification status is always displayed on the [NFDI4Earth Label verification website](https://label.nfdi4earth.de/verify). The status reads *Not certified* for repositories that did never apply for the Label or that did not meet the requirements during the assessment. It reads *Certified* for other repositories.



## Additional resources

* The NFDI4Earth Label Website (WIP)
* The FAIR Connect publication on the NFDI4Earth Label (WIP)
* The official [Guidelines and Metrics document of the NFDI4Earth Label](https://label.nfdi4earth.de/guidelines)

# Copyright 2023 SGN
#
# SPDX-License-Identifier: Apache-2.0

"""pytest configuration script for nfdi4earth-label."""

import pytest  # noqa: F401

#!/bin/bash
# Add some assessments to the database for testing purposes


repository_id="n4e/r3d-r3d100010134"   # PANGAEA

# insert some re3data assessments. These are real ones, computed for the repo.
lbl_runassessment re3data "${repository_id}" --store # Run a real assessment, based on the data in the database.
lbl_runassessment re3data "${repository_id}" --store  # a 2nd real one, only difference is the creation time.
lbl_saveassessment re3data n4e/r3d-r3d100010134   # explicitely add one with negative result
lbl_saveassessment re3data n4e/r3d-r3d100010134  --achieved  # explicitely add one with negative result
lbl_saveassessment re3data n4e/r3d-r3d100010134  --random  # add one with random results in all metrics
lbl_saveassessment re3data n4e/r3d-r3d100010134  --random  # add one with random results in all metrics

# And some F-UJI (sub) assessments:
# IMPORTANT: This requires that the F-UJI service is running and accessible! If not,
# the assessments will be inserted, but the results will be empty/failed and thus negative.
lbl_runassessment fuji "${repository_id}" --store --apiurl auto  # Run real F-UJI assessment
lbl_saveassessment fuji "${repository_id}" --achieved   # positive result
lbl_saveassessment fuji "${repository_id}" --random   # random results
lbl_saveassessment fuji "${repository_id}" --random   # random results


# And, finally, some (sub) assessments of type selfassessment:
lbl_saveassessment selfassessment "${repository_id}"              # negative result
lbl_saveassessment selfassessment "${repository_id}" --achieved   # positive result
lbl_saveassessment selfassessment "${repository_id}" --random   # random results
lbl_saveassessment selfassessment "${repository_id}" --random   # random results
